function maquinas(){
    docker compose -f build/docker-compose.yml down 
    docker compose up
}

if [[ -f ".ready" ]]; then
    echo "Pronto para iniciar o ambiente... Vamos ver o que acontece!"
   
    #Atualizando o pgAdmin4 e o CI para ter sempre a ultima versão
    docker pull enyalius/ci
    maquinas    
else
    ENY='eny'
    echo "Parece que é nossa primeira execução vamos realizar algumas tarefas. Pegue um café e curta!"
    echo "--------"
    echo "Se acontecer algum erro por aqui verifique se você tem o git devidamente instalado em seu computador."
    #comandos de cima remover após aprender decentemente o funcionamento do composer.

    mkdir -p z_data/classes
    
    # clear 
    echo "Passando o controle para o Eny... "
    $ENY gitconfig 

    echo "Baixando as libs PHP... "
    $ENY composer install --ignore-platform-reqs
    echo "Baixando as libs JS... "
    $ENY yarn --production=true --modules-folder=./public_html/vendor
    
    echo 'Agora vou compilar as deps do Enyalius \0/'

    $ENY -gc distjs 
    $ENY -gc css

    echo "Pronto para iniciar o ambiente."
    maquinas

    curl -k https://localhost/login/tool/InstallBD 
    touch .ready  
fi
