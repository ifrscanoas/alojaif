const handleSubmit = function(event) {
    const jaTemFoto = $(this).attr('data-tem-foto')
    event.preventDefault();
    const id = $(this).attr('data-id')
    const foto = event.target.files[0];
    const newImage = new Image();
    newImage.src = URL.createObjectURL(foto);
    const data = new FormData();
    data.append('foto', foto);

    $('#loading').show()
    $(`#foto${id}`).hide()

    // $.ajax({
    //     // Your server script to process the upload
    //     url: '/admin//abrigado/criarNovoFim',
    //     type: 'POST',
    
    //     // Form data
    //     data: new FormData($('#dropzone' + id)[0]),
    
    //     // Tell jQuery not to process data or worry about content-type
    //     // You *must* include these options!
    //     cache: false,
    //     contentType: false,
    //     processData: false,
    
    //     // Custom XMLHttpRequest
    //     xhr: function () {
    //       var myXhr = $.ajaxSettings.xhr();
    //       if (myXhr.upload) {
    //         // For handling the progress of the upload
    //         myXhr.upload.addEventListener('progress', function (e) {
    //           if (e.lengthComputable) {
    //             $('progress').attr({
    //               value: e.loaded,
    //               max: e.total,
    //             });
    //           }
    //         }, false);
    //       }
    //       return myXhr;
    //     }
    //   });





    $.ajax({
        type: 'POST',
        url:'/admin/abrigado/enviarFoto/' + id,
        data: data,
        processData: false, 
        contentType: false, 
        success: function(response) {
            if (response) {
                $('#loading').hide()
                if (jaTemFoto == 'true') {
                    $(`#foto${id}`).attr('src', newImage.src)
                    $(`#foto${id}`).show()
                }
                else {
                    $(`#divFoto${id}`).hide()
                    $(`#foto${id}`).attr('src', newImage.src)
                    $(`#foto${id}`).show()
                }
            }
            else {
                $('#loading').hide()
                $(`#foto${id}`).show()
                alert('Erro ao enviar a foto')
            }
         }
    });
  };

  $('.inputFoto').on('change', handleSubmit)