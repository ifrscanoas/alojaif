const CONF = {
    url: '/chatRequest'
}

$(document).ready(function(){

    message('Bem vindo. Qual a sua dúvida sobre os dados?', 'other')
               

    $("#chatSend").submit(function (e) { 
        e.preventDefault();
        let prompt = $("#chatPrompt").val();
        $("#chatPrompt").val("");
        message(prompt, 'me')
        showLoading()
        $.post(CONF.url ,
            'prompt=' + prompt 
            ,function(data){
                let obj = JSON.parse(data);
                hideLoading();
                message(obj.response, 'other', obj.type)
            }
        )
    });
});

function showLoading(){
    $(".chats").append(
        `<li id="skeleto">
        <div class="spinner-grow spinner-grow-sm" role="status">
        <span class="sr-only">Loading...</span>
      </div>
      <div class="spinner-grow spinner-grow-sm" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow spinner-grow-sm" role="status">
  <span class="sr-only">Loading...</span>
</div>
      </li>`
    )

}

function hideLoading(){
    $("#skeleto").remove()
}

function message(text, sender = 'me', tipo='text'){
    let content = ''
    if(sender == 'me'){
        content = me(text, tipo)
    }else{
        content = other(text, tipo)
    }
    $(".chats").append(content)
}

function me(text, tipo){
    return `<li class="by-other">
                <!-- Use the class "pull-left" in avatar
                <div class="avatar pull-left">
                    <img src="/" alt="foto do usuário">
                </div> -->

                <div class="chat-content">
                <div class="chat-meta">Eu  <span class="pull-right"></span></div>
                ${processa(text, tipo)}
                <div class="clearfix"></div>
                </div>
            </li>`
}

function other(text, tipo){
    return `   <li class="by-me">
    <!-- Use the class "pull-right" in avatar
    <div class="avatar pull-left">
        <img src="/" alt="foto do usuário">
    </div> -->

    <div class="chat-content">
    <!-- In the chat meta, first include "time" then "name" -->
    <div class="chat-meta"> Gepeto da esperança <span class="pull-right"></span></div>
    ${processa(text, tipo)}
    <div class="clearfix"></div>
    </div>
</li>`
}

function processa(texto, tipo){
    if(tipo == 'image'){
        return `<img src="data:image/png;base64,${texto}" alt="Imagem gerada via IA" />`
    }else{
        return texto
    }
}