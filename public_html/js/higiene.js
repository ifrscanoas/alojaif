$(document).ready(function(){
    $(".addRegistroHigiene").on("click", function(){
        let idPessoa = $(this).data('id');
        let idTipoRegistro = $(this).data('tipo');

        exibirTelaComplemento(`<h2>Adicionar um entrega de produto de higiene</h2>
            <p class="help"> Todos os campos são opcionais </p>
            <label> Quantidade
            <input name="quantidade" id="qtd" type="number" class="form-control" value="1">
            </label> 
            <br />
            <label> Digite uma observação ou descrição do Registro
            <textarea name="observacao" id="observacao" class="form-control"></textarea>
            </label>
      
        `, idPessoa, idTipoRegistro, function(){
            const date = new Date();

            $("#list" + idTipoRegistro + idPessoa).append(
                `
                    <li> ${$("#observacao").val()} - ${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}</li>
                `
            );
        })
    });


})