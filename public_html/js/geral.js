//mantem logado caso tiver atualização ou qualquer coisa redireciona para o login
setInterval(function(){
    $.get('/login/mantemVivo', function(data){
      console.log(data)
      if(data=="redirect"){
        document.location = '/'
      }
    })
  }, 
  30000
  ); 

function now(){
  const date = new Date();

  let mes = date.getMonth()+1;
  mes = mes<10? `0${mes}`: mes;

  return `${date.getDate()}/${mes}/${date.getFullYear()}`;
}