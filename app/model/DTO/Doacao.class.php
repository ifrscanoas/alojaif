<?php
/**
 * Classe para a transferencia de dados de Doacao entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 23-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Doacao implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $pessoaId;
    private $tipoRegistroId;
    private $dataRegistro;
    private $observacao;
    private $quantidade;
    private $isValid;
    private $table;

    private $pessoa;
    private $tipoRegistro;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.doacao')
    {
        $this->table = $table;
        $this->ignoreField('pessoa', 'tipoRegistro');
        $this->dataRegistro = date('Y-m-d h:i:s');
    }

    /**
     * Método que seta o valor da variável pessoaId
     *
     * @param int $pessoaId - Valor da variável pessoaId
     */
    public function setPessoaId($pessoaId)
    {
        if(empty($pessoaId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Pessoa id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($pessoaId) && is_int($pessoaId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Pessoa id não é um número inteiro válido!';
            return false;
        }
        $this->pessoaId = $pessoaId;
        return $this;
    }

    /**
     * Método que seta o valor da variável tipoRegistroId
     *
     * @param int $tipoRegistroId - Valor da variável tipoRegistroId
     */
    public function setTipoRegistroId($tipoRegistroId)
    {
        if(empty($tipoRegistroId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo registro id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($tipoRegistroId) && is_int($tipoRegistroId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo registro id não é um número inteiro válido!';
            return false;
        }
        $this->tipoRegistroId = $tipoRegistroId;
        return $this;
    }

    /**
     * Retorna o valor da variável dataRegistro formatada 
     *
     * @param bool $comHora - opção para mostrar a hora ou não 
     * @param bool $extenso - opção para retornar a data por extenso ou não 
     * @return string - Valor da variável dataRegistro formatada 
     */
    public function getDataRegistroFormatada($comHora = true, $extenso = true)
    {
        return $extenso ? DateUtil::formataDataExtenso($this->dataRegistro, $comHora) : DateUtil::formataData($this->dataRegistro, $comHora);
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }
}
