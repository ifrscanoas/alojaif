<?php

use SebastianBergmann\Type\FalseType;

/**
 * Classe para a transferencia de dados de Pet entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 17-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Pet implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $abrigadoId;
    private $tipo;
    private $nome;
    private $raca;
    private $descricao;
    private $foto;
    private $perdido;
    private $resgatado;
    private $sexo;
    private $vacinas;
    private $tipoPetId;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.pet')
    {
        $this->table = $table;
    }

    public function getTipo($tipos = false){
        if(isset($tipos[$this->getTipoPetId()])){
            return $tipos[$this->getTipoPetId()]->getTipo();
        }
        return $this->tipo;
    }

    /**
     * Método que seta o valor da variável abrigadoId
     *
     * @param int $abrigadoId - Valor da variável abrigadoId
     */
    public function setAbrigadoId($abrigadoId)
    {
        if(empty($abrigadoId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Abrigado id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($abrigadoId) && is_int($abrigadoId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Abrigado id não é um número inteiro válido!';
            return false;
        }
        $this->abrigadoId = $abrigadoId;
        return $this;
    }

    /**
     * Método que seta o valor da variável perdido
     *
     * @param bool $perdido - Valor da variável perdido
     */
    public function setPerdido($perdido)
    {
        if (empty($perdido) || $perdido === 'f') {
            $this->perdido = false;
        } else {
            $this->perdido = true;
        }
        return $this;
    }

    /**
     * Método que seta o valor da variável resgatado
     *
     * @param bool $resgatado - Valor da variável resgatado
     */
    public function setResgatado($resgatado)
    {
        if (empty($resgatado) || $resgatado === 'f') {
            $this->resgatado = false;
        } else {
            $this->resgatado = true;
        }
        return $this;
    }

    /**
     * Método que seta o valor da variável vacinas
     *
     * @param bool $vacinas - Valor da variável vacinas
     */
    public function setVacinas($vacinas)
    {
        if (empty($vacinas) || $vacinas === 'f') {
            $this->vacinas = false;
        } else {
            $this->vacinas = true;
        }
        return $this;
    }

    /**
     * Método que seta o valor da variável tipoPetId
     *
     * @param int $tipoPetId - Valor da variável tipoPetId
     */
    public function setTipoPetId($tipoPetId)
    {
        if(!(is_numeric($tipoPetId) && is_int($tipoPetId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo pet id não é um número inteiro válido!';
            return false;
        }
        $this->tipoPetId = $tipoPetId;
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
    }
}
