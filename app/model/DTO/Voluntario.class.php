<?php
/**
 * Classe para a transferencia de dados de Voluntario entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 10-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Voluntario implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $nomeCompleto;
    private $cpf;
    private $telefone;
    private $foto;
    private $obs;
    private $isValid;
    private $table;
    private $tipoVoluntarioId;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.voluntario')
    {
        $this->table = $table;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }
}
