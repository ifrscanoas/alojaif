<?php
/**
 * Classe para a transferencia de dados de Pessoa entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 23-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Pessoa implements DTOInterface, JsonSerializable
{
    use core\model\DTOTrait;

    private $id;
    private $tipoPessoaId;
    private $cpf;
    private $observacao;
    private $nomeCompleto;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.pessoa')
    {
        $this->table = $table;
    }

    /**
     * Método que seta o valor da variável tipoPessoaId
     *
     * @param int $tipoPessoaId - Valor da variável tipoPessoaId
     */
    public function setTipoPessoaId($tipoPessoaId)
    {
        if(empty($tipoPessoaId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo pessoa id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($tipoPessoaId) && is_int($tipoPessoaId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo pessoa id não é um número inteiro válido!';
            return false;
        }
        $this->tipoPessoaId = $tipoPessoaId;
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }
}
