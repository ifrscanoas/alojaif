<?php
/**
 * Classe para a transferencia de dados de Registro entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 12-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Registro implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $abrigadoId;
    private $tipoRegistroId;
    private $observacao;
    private $tamanho;
    private $quantidade;
    private $dataRegistro;
    private $valido;
    private $isValid;
    private $table;

    private ?TipoRegistro $tipoRegistro = null;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.registro')
    {
        $this->table = $table;
        $this->ignoreField('tipoRegistro', 'dataRegistro');
    }

    /**
     * Método que seta o valor da variável abrigadoId
     *
     * @param int $abrigadoId - Valor da variável abrigadoId
     */
    public function setAbrigadoId($abrigadoId)
    {
        if(empty($abrigadoId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Abrigado id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($abrigadoId) && is_int($abrigadoId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Abrigado id não é um número inteiro válido!';
            return false;
        }
        $this->abrigadoId = $abrigadoId;
        return $this;
    }
    public function getTipoRegistro(){
        if($this->tipoRegistro == null){
            $this->tipoRegistro = TipoRegistro::getOne($this->tipoRegistroId);
        }
        return $this->tipoRegistro;
    }
    /**
     * Método que seta o valor da variável tipoRegistroId
     *
     * @param int $tipoRegistroId - Valor da variável tipoRegistroId
     */
    public function setTipoRegistroId($tipoRegistroId)
    {
        if(empty($tipoRegistroId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo registro id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($tipoRegistroId) && is_int($tipoRegistroId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo registro id não é um número inteiro válido!';
            return false;
        }
        $this->tipoRegistroId = $tipoRegistroId;
        return $this;
    }

    /**
     * Retorna o valor da variável dataRegistro formatada 
     *
     * @param bool $comHora - opção para mostrar a hora ou não 
     * @param bool $extenso - opção para retornar a data por extenso ou não 
     * @return string - Valor da variável dataRegistro formatada 
     */
    public function getDataRegistroFormatada($comHora = true, $extenso = true)
    {
        return $extenso ? DateUtil::formataDataExtenso($this->dataRegistro, $comHora) : DateUtil::formataData($this->dataRegistro, $comHora);
    }

    /**
     * Método que seta o valor da variável valido
     *
     * @param bool $valido - Valor da variável valido
     */
    public function setValido($valido)
    {
        if (empty($valido) || $valido === 'f') {
            $this->valido = false;
        } else {
            $this->valido = true;
        }
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    public function setID($id) {
        $this->id = $id;
    }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id='.$this->id;
     }

}
