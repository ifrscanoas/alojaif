<?php
/**
 * Classe para a transferencia de dados de Presenca entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 11-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Presenca implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $voluntarioId;
    private $presencaId;
    private $dataRegistro;
    private $entrada;
    private $saida;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.presenca')
    {
        $this->table = $table;
    }

    /**
     * Método que seta o valor da variável voluntarioId
     *
     * @param int $voluntarioId - Valor da variável voluntarioId
     */
    public function setVoluntarioId($voluntarioId)
    {
        if(empty($voluntarioId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Voluntario id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($voluntarioId) && is_int($voluntarioId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Voluntario id não é um número inteiro válido!';
            return false;
        }
        $this->voluntarioId = $voluntarioId;
        return $this;
    }

    /**
     * Método que seta o valor da variável presencaId
     *
     * @param int $presencaId - Valor da variável presencaId
     */
    public function setPresencaId($presencaId)
    {
        if(empty($presencaId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Presenca id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($presencaId) && is_int($presencaId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Presenca id não é um número inteiro válido!';
            return false;
        }
        $this->presencaId = $presencaId;
        return $this;
    }

    /**
     * Retorna o valor da variável dataRegistro formatada 
     *
     * @param bool $comHora - opção para mostrar a hora ou não 
     * @param bool $extenso - opção para retornar a data por extenso ou não 
     * @return string - Valor da variável dataRegistro formatada 
     */
    public function getDataRegistroFormatada($comHora = true, $extenso = true)
    {
        return $extenso ? DateUtil::formataDataExtenso($this->dataRegistro, $comHora) : DateUtil::formataData($this->dataRegistro, $comHora);
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }
}
