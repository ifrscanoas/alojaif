<?php
/**
 * Classe para a transferencia de dados de Papel entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 22-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Papel implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $usuario;
    private $senha;
    private $nomePapel;
    private $rotaBase;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.papel')
    {
        $this->table = $table;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }
}
