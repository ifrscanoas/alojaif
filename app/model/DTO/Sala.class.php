<?php
/**
 * Classe para a transferencia de dados de Sala entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 10-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Sala implements DTOInterface
{
    use core\model\DTOTrait;

    public $id;
    public $bloco;
    public $sala;
    private $capacidade;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.sala')
    {
        $this->table = $table;
    }

    /**
     * Método que seta o valor da variável capacidade
     *
     * @param string $capacidade - Valor da variável capacidade
     */
    public function setCapacidade($capacidade)
    {
        $capacidade = str_replace(',', '.', $capacidade);
        if(!is_numeric($capacidade)){
            $GLOBALS['ERROS'][] = 'O valor informado em  Capacidade não é um número válido!';
            return false;
        }
        $this->capacidade = $capacidade;
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }



}
