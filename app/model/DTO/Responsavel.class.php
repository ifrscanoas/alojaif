<?php
/**
 * Classe para a transferencia de dados de Responsavel entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 17-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Responsavel implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $abrigadoId;
    private $responsavelId;
    private $tipoResponsavelId;
    private $observacao;
    private $isValid;
    private $table;
    private $abrigado;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.responsavel')
    {
        $this->table = $table;
        $this->ignoreField('abrigado');
    }


    public function getTipoResponsavel($tiposResponsaveis){
        return $tiposResponsaveis[$this->tipoResponsavelId];
    }

    /**
     * Método que seta o valor da variável abrigadoId
     *
     * @param int $abrigadoId - Valor da variável abrigadoId
     */
    public function setAbrigadoId($abrigadoId)
    {
        if(empty($abrigadoId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Abrigado id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($abrigadoId) && is_int($abrigadoId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Abrigado id não é um número inteiro válido!';
            return false;
        }
        $this->abrigadoId = $abrigadoId;
        return $this;
    }

    /**
     * Método que seta o valor da variável responsavelId
     *
     * @param int $responsavelId - Valor da variável responsavelId
     */
    public function setResponsavelId($responsavelId)
    {
        if(empty($responsavelId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Responsavel id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($responsavelId) && is_int($responsavelId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Responsavel id não é um número inteiro válido!';
            return false;
        }
        $this->responsavelId = $responsavelId;
        return $this;
    }

    /**
     * Método que seta o valor da variável tipoResponsavelId
     *
     * @param int $tipoResponsavelId - Valor da variável tipoResponsavelId
     */
    public function setTipoResponsavelId($tipoResponsavelId)
    {
        if(empty($tipoResponsavelId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo responsavel id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($tipoResponsavelId) && is_int($tipoResponsavelId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo responsavel id não é um número inteiro válido!';
            return false;
        }
        $this->tipoResponsavelId = $tipoResponsavelId;
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }
}
