<?php
/**
 * Classe para a transferencia de dados de Compra entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 25-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Compra implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $notaFiscal;
    private $dataCompra;
    private $valor;
    private $quantidade;
    private $item;
    private $observacao;
    private $razaoSocialBeneficiado;
    private $nomeFantasia;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.compra')
    {
        $this->table = $table;
    }

    /**
     * Retorna o valor da variável dataCompra formatada 
     *
     * @param bool $comHora - opção para mostrar a hora ou não 
     * @param bool $extenso - opção para retornar a data por extenso ou não 
     * @return string - Valor da variável dataCompra formatada 
     */
    public function getDataCompraFormatada($comHora = true, $extenso = true)
    {
        return $extenso ? DateUtil::formataDataExtenso($this->dataCompra, $comHora) : DateUtil::formataData($this->dataCompra, $comHora);
    }

    /**
     * Método que seta o valor da variável dataCompra
     *
     * @param string $dataCompra - Valor da variável dataCompra
     */
    public function setDataCompra($dataCompra)
    {
        if(empty($dataCompra)){
            $GLOBALS['ERROS'][] = 'O valor informado em Data compra não pode ser nulo!';
            return false;
        }
        $this->dataCompra = $dataCompra;
        return $this;
    }

    /**
     * Método que seta o valor da variável valor
     *
     * @param string $valor - Valor da variável valor
     */
    public function setValor($valor)
    {
        if(empty($valor)){
            $GLOBALS['ERROS'][] = 'O valor informado em Valor não pode ser nulo!';
            return false;
        }
        $valor = str_replace(',', '.', $valor);
        if(!is_numeric($valor)){
            $GLOBALS['ERROS'][] = 'O valor informado em  Valor não é um número válido!';
            return false;
        }
        $this->valor = $valor;
        return $this;
    }

    /**
     * Método que seta o valor da variável quantidade
     *
     * @param int $quantidade - Valor da variável quantidade
     */
    public function setQuantidade($quantidade)
    {
        if(!(is_numeric($quantidade) && is_int($quantidade + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Quantidade não é um número inteiro válido!';
            return false;
        }
        $this->quantidade = $quantidade;
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }
}
