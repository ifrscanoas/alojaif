<?php
/**
 * Classe para a transferencia de dados de TipoRegistro entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 15-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class TipoRegistro implements DTOInterface, JsonSerializable
{
    use core\model\DTOTrait;

    private $id;
    private $tipo;
    private $sys = false;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.tipo_registro')
    {
        $this->table = $table;
    }

    /**
     * Método que seta o valor da variável sys
     *
     * @param bool $sys - Valor da variável sys
     */
    public function setSys($sys)
    {
        if (empty($sys) || $sys === 'f') {
            $this->sys = false;
        } else {
            $this->sys = true;
        }
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->id,
            'label' => $this->tipo
        ];
    }
}
