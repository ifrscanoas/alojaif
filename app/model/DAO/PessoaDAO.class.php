<?php

/**
 * Classe de modelo referente ao objeto Pessoa para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 23-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class PessoaDAO extends AbstractDAO 
{

    /**
    * Construtor da classe PessoaDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Pessoa::table();
        $this->colunmID = 'id';
        $this->colunms = [     'tipo_pessoa_id',
                                'cpf',
                                'observacao',
                                'nome_completo'
                          ];
    }

    /**
     * Retorna um objeto setado Pessoa
     * com objetivo de servir as funções getTabela, getLista e getPessoa
     *
     * @param array $dados
     * @return objeto Pessoa
     */
    protected function setDados($dados)
    {
        $pessoa = new Pessoa();
        $pessoa->setId($dados['principal']);
        $pessoa->setTipoPessoaId($dados['tipo_pessoa_id']);
        $pessoa->setCpf($dados['cpf']);
        $pessoa->setObservacao($dados['observacao']);
        $pessoa->setNomeCompleto($dados['nome_completo']);
        return $pessoa;
    }
}