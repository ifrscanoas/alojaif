<?php

/**
 * Classe de modelo referente ao objeto TipoRegistro para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 15-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class TipoRegistroDAO extends AbstractDAO 
{

    /**
    * Construtor da classe TipoRegistroDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  TipoRegistro::table();
        $this->colunmID = 'id';
        $this->colunms = [     'tipo',
                                'sys'
                          ];
    }

    /**
     * Retorna um objeto setado TipoRegistro
     * com objetivo de servir as funções getTabela, getLista e getTipoRegistro
     *
     * @param array $dados
     * @return objeto TipoRegistro
     */
    protected function setDados($dados)
    {
        $tipoRegistro = new TipoRegistro();
        $tipoRegistro->setId($dados['principal']);
        $tipoRegistro->setTipo($dados['tipo']);
        $tipoRegistro->setSys($dados['sys']);
        return $tipoRegistro;
    }
}