<?php

/**
 * Classe de modelo referente ao objeto Pet para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 17-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class PetDAO extends AbstractDAO 
{

    /**
    * Construtor da classe PetDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Pet::table();
        $this->colunmID = 'id';
        $this->colunms = [     'abrigado_id',
                                'tipo',
                                'nome',
                                'raca',
                                'descricao',
                                'foto',
                                'perdido',
                                'resgatado',
                                'sexo',
                                'vacinas',
                                'tipo_pet_id'
                          ];
    }

    /**
     * Retorna um objeto setado Pet
     * com objetivo de servir as funções getTabela, getLista e getPet
     *
     * @param array $dados
     * @return objeto Pet
     */
    protected function setDados($dados)
    {
        $pet = new Pet();
        $pet->setId($dados['principal']);
        $pet->setAbrigadoId($dados['abrigado_id']);
        $pet->setTipo($dados['tipo']);
        $pet->setNome($dados['nome']);
        $pet->setRaca($dados['raca']);
        $pet->setDescricao($dados['descricao']);
        $pet->setFoto($dados['foto']);
        $pet->setPerdido($dados['perdido']);
        $pet->setResgatado($dados['resgatado']);
        $pet->setSexo($dados['sexo']);
        $pet->setVacinas($dados['vacinas']);
        $pet->setTipoPetId($dados['tipo_pet_id']);
        return $pet;
    }

    public function getAllByAbrigados(){
        //Usando subquery talvez pensar em umas solução melhor para o sistema Ou profilar para ver o que se aplica mais
        return $this->getList('abrigado_id in (SELECT id FROM abrigado WHERE situacao_id NOT in (3,7,8,10))');
    }

    public function getMapByAbrigados(){
        $lista = [];
        $result = $this->getList('abrigado_id in (SELECT id FROM abrigado WHERE situacao_id NOT in (3,7,8,10))');

        foreach($result as $pet){
            $lista[$pet->getAbrigadoId()][] = $pet;
        }
        return $lista;
    }

    public function getAllByCampus(){
        //Usando subquery talvez pensar em umas solução melhor para o sistema Ou profilar para ver o que se aplica mais
        return $this->getList('abrigado_id = 10021');
    }
}