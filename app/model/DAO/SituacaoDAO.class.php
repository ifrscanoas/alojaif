<?php

/**
 * Classe de modelo referente ao objeto Situacao para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 12-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class SituacaoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe SituacaoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Situacao::table();
        $this->colunmID = 'id';
        $this->colunms = [     'situacao',
                                'cor'
                          ];
    }

    /**
     * Retorna um objeto setado Situacao
     * com objetivo de servir as funções getTabela, getLista e getSituacao
     *
     * @param array $dados
     * @return objeto Situacao
     */
    protected function setDados($dados)
    {
        $situacao = new Situacao();
        $situacao->setId($dados['principal']);
        $situacao->setSituacao($dados['situacao']);
        $situacao->setCor($dados['cor']);
        return $situacao;
    }
}