<?php

/**
 * Classe de modelo referente ao objeto TipoPessoa para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 23-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class TipoPessoaDAO extends AbstractDAO 
{

    /**
    * Construtor da classe TipoPessoaDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  TipoPessoa::table();
        $this->colunmID = 'id';
        $this->colunms = [     'tipo_pessoa',
                                'cor'
                          ];
    }

    /**
     * Retorna um objeto setado TipoPessoa
     * com objetivo de servir as funções getTabela, getLista e getTipoPessoa
     *
     * @param array $dados
     * @return objeto TipoPessoa
     */
    protected function setDados($dados)
    {
        $tipoPessoa = new TipoPessoa();
        $tipoPessoa->setId($dados['principal']);
        $tipoPessoa->setTipoPessoa($dados['tipo_pessoa']);
        $tipoPessoa->setCor($dados['cor']);
        return $tipoPessoa;
    }
}