<?php

/**
 * Classe de modelo referente ao objeto Abrigado para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 11-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class AbrigadoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe AbrigadoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Abrigado::table();
        $this->colunmID = 'id';
        $this->colunms = [     'nome_completo',
                                'cpf',
                                'observacao',
                                'documento',
                                'cartao_sus',
                                'foto',
                                'data_nascimento',
                                'idade_old',
                                'endereco',
                                'bairro',
                                'telefone',
                                'saida_temporaria',
                                'data_saida',
                                'situacao_id',
                                'sala_id'
                          ];
    }

    public function getResponsaveis(int $idAbrigado){
        $sql = 'SELECT a.id as principal, a.*, r.id as respid, tipo_responsavel_id FROM abrigado a INNER JOIN responsavel r ON a.id = r.responsavel_id
        WHERE  r.abrigado_id =  '.$idAbrigado;
        $lista= [];
        $query = $this->query($sql);
        if ($query) {
            foreach ($query as $linhaBanco) {
                $responsavel = new Responsavel();
                $abrigado = $this->setDados($linhaBanco);
                $responsavel->setID($linhaBanco['respid']);
                $responsavel->setAbrigado($abrigado);
                $responsavel->setTipoResponsavelId($linhaBanco['tipo_responsavel_id']);
                $responsavel->setAbrigadoId($abrigado->getId());
                
                $lista[] =$responsavel;
            }
        }


        return $lista;
    }


    private $consumidos ;
    private $familia;
    public function getFamilias()
    {
        $familias = [];
        $abrigados = $this->getAbrigadosComFamilia();
        $this->consumidos = [];

        foreach($abrigados as $chave => $valor){
            if(in_array($chave, $this->consumidos)){
                continue ;
            }
            $this->familia = [
                $chave => $valor
            ];
            $this->processaFilhos($chave);
            $this->consumidos[] = $chave;

            $familias[] = $this->familia;
         }

        return $familias;

    }

    private function processaFilhos($id){
        if(in_array($id, $this->consumidos)){
            return ;
        }     
        $this->consumidos[] = $id;
        $todos = $this->todosRelacionados($id);   
        //consulta os parentescos
        foreach($todos as $relacionado){
            $this->processaFilhos($relacionado['id']);
            $this->familia[$relacionado['id']] = $this->setDados($relacionado);
        }

    }

    public function todosRelacionados(int $id){
        return $this->queryTable('abrigado', 'DISTINCT id as principal, *',
        "id in(select responsavel_id from responsavel WHERE abrigado_id = $id) 
				OR id in (select abrigado_id from responsavel WHERE responsavel_id = $id)"
    );
    }

    public function getAbrigadosComFamilia($order = 'sala_id, data_nascimento ASC'){
        $condicao = 'situacao_id NOT IN (3,7,8,13)
        AND id IN (
            SELECT distinct id FROM abrigado where id IN (SELECT abrigado_id FROM responsavel) 
        OR id IN (SELECT responsavel_id from responsavel))'; 
        return $this->getList($condicao, $order, false, 'principal');
    }

    public function getAbrigadosSemFamilia($order = false){
        $condicao = 'situacao_id NOT IN (3,7,8,13)
        AND id NOT IN (
            SELECT distinct id FROM abrigado where id IN (select abrigado_id FROM responsavel) 
        OR id IN (SELECT responsavel_id FROM responsavel))'; 
        return $this->getList($condicao, $order);
    }

    /**
     * Retorna um objeto setado Abrigado
     * com objetivo de servir as funções getTabela, getLista e getAbrigado
     *
     * @param array $dados
     * @return objeto Abrigado
     */
    protected function setDados($dados)
    {
        $abrigado = new Abrigado();
        $abrigado->setId($dados['principal']);
        $abrigado->setNomeCompleto($dados['nome_completo']);
        $abrigado->setCpf($dados['cpf']);
        $abrigado->setObservacao($dados['observacao']);
        $abrigado->setDocumento($dados['documento']);
        $abrigado->setCartaoSus($dados['cartao_sus']);
        $abrigado->setFoto($dados['foto']);
        $abrigado->setDataNascimento($dados['data_nascimento']);
        $abrigado->setIdadeOld($dados['idade_old']);
        $abrigado->setEndereco($dados['endereco']);
        $abrigado->setBairro($dados['bairro']);
        $abrigado->setTelefone($dados['telefone']);
        $abrigado->setSaidaTemporaria($dados['saida_temporaria']);
        $abrigado->setDataSaida($dados['data_saida']);
        $abrigado->setSituacaoId($dados['situacao_id']);
        $abrigado->setSalaId($dados['sala_id']);
        return $abrigado;
    }
}