<?php

/**
 * Classe de modelo referente ao objeto Presenca para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 11-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class PresencaDAO extends AbstractDAO 
{

    /**
    * Construtor da classe PresencaDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Presenca::table();
        $this->colunmID = 'id';
        $this->colunms = [     'voluntario_id',
                                'presenca_id',
                                'data_registro',
                                'entrada',
                                'saida'
                          ];
    }

    /**
     * Retorna um objeto setado Presenca
     * com objetivo de servir as funções getTabela, getLista e getPresenca
     *
     * @param array $dados
     * @return objeto Presenca
     */
    protected function setDados($dados)
    {
        $presenca = new Presenca();
        $presenca->setId($dados['principal']);
        $presenca->setVoluntarioId($dados['voluntario_id']);
        $presenca->setPresencaId($dados['presenca_id']);
        $presenca->setDataRegistro($dados['data_registro']);
        $presenca->setEntrada($dados['entrada']);
        $presenca->setSaida($dados['saida']);
        return $presenca;
    }
}