<?php

/**
 * Classe de modelo referente ao objeto TipoVoluntario para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 27-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class TipoVoluntarioDAO extends AbstractDAO 
{

    /**
    * Construtor da classe TipoVoluntarioDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  TipoVoluntario::table();
        $this->colunmID = 'id';
        $this->colunms = [     'tipo'
                          ];
    }

    /**
     * Retorna um objeto setado TipoVoluntario
     * com objetivo de servir as funções getTabela, getLista e getTipoVoluntario
     *
     * @param array $dados
     * @return objeto TipoVoluntario
     */
    protected function setDados($dados)
    {
        $tipoVoluntario = new TipoVoluntario();
        $tipoVoluntario->setId($dados['principal']);
        $tipoVoluntario->setTipo($dados['tipo']);
        return $tipoVoluntario;
    }
}