<?php

/**
 * Classe de modelo referente ao objeto DoacaoRecebimento para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 26-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class DoacaoRecebimentoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe DoacaoRecebimentoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  DoacaoRecebimento::table();
        $this->colunmID = 'id';
        $this->colunms = [     'pessoa_id',
                                'tipo_doacao_id',
                                'data_recebimento',
                                'valor',
                                'quantidade',
                                'obs'
                          ];
    }

    /**
     * Retorna um objeto setado DoacaoRecebimento
     * com objetivo de servir as funções getTabela, getLista e getDoacaoRecebimento
     *
     * @param array $dados
     * @return objeto DoacaoRecebimento
     */
    protected function setDados($dados)
    {
        $doacaoRecebimento = new DoacaoRecebimento();
        $doacaoRecebimento->setId($dados['principal']);
        $doacaoRecebimento->setPessoaId($dados['pessoa_id']);
        $doacaoRecebimento->setTipoDoacaoId($dados['tipo_doacao_id']);
        $doacaoRecebimento->setDataRecebimento($dados['data_recebimento']);
        $doacaoRecebimento->setValor($dados['valor']);
        $doacaoRecebimento->setQuantidade($dados['quantidade']);
        $doacaoRecebimento->setObs($dados['obs']);
        return $doacaoRecebimento;
    }


    public function getListaCompleta($condicao = false, $order = false, $limit = false, $offset = false)
    {

        $data = $this->queryTable('doacao_recebimento_completa', 'id as principal, *', $condicao, $order, $limit, $offset);
        $result = [];
        foreach ($data as $linha) {        
            $doacao = $this->setDados($linha);
            $pessoa = new Pessoa();
            $pessoa->setId($linha['pessoa_id']);
            $pessoa->setNomeCompleto($linha['nome_completo']);
            $pessoa->setCpf($linha['cpf']);

            $doacao->setPessoa($pessoa);

            $tipoRegistro = new TipoRegistro();
            $tipoRegistro->setId($linha['tipo_doacao_id']);
            $tipoRegistro->setTipo($linha['tipo']);
            
            $doacao->setTipoRegistro($tipoRegistro);

            $result[$linha['principal']] = $doacao;
        }
        return $result;
    }
}