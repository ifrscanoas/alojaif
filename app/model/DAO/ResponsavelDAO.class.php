<?php

/**
 * Classe de modelo referente ao objeto Responsavel para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 17-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ResponsavelDAO extends AbstractDAO 
{

    /**
    * Construtor da classe ResponsavelDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Responsavel::table();
        $this->colunmID = 'id';
        $this->colunms = [     'abrigado_id',
                                'responsavel_id',
                                'tipo_responsavel_id',
                                'observacao'
                          ];
    }

    /**
     * Retorna um objeto setado Responsavel
     * com objetivo de servir as funções getTabela, getLista e getResponsavel
     *
     * @param array $dados
     * @return objeto Responsavel
     */
    protected function setDados($dados)
    {
        $responsavel = new Responsavel();
        $responsavel->setId($dados['principal']);
        $responsavel->setAbrigadoId($dados['abrigado_id']);
        $responsavel->setResponsavelId($dados['responsavel_id']);
        $responsavel->setTipoResponsavelId($dados['tipo_responsavel_id']);
        $responsavel->setObservacao($dados['observacao']);
        return $responsavel;
    }
}