<?php

class SituacaoFragment
{

    private $listaSituacao;

    public function __construct($listaSituacao = false)
    {
        if(!$listaSituacao){
            $listaSituacao = Situacao::getMap();
        }
        $this->listaSituacao = $listaSituacao;
    }

    public function getCor($id)
    {
        return $this->listaSituacao[$id]->getCor();
    }

    public function getLabel($id)
    {
        return $this->listaSituacao[$id]->getSituacao();
    }
}
