<body>
<header id="header">
    <a href="{if isset($SESSAO['principal'])}{$SESSAO['principal']}{else}/{/if}" class="parteLogo"> 

        {* <div class="menuIcon"><img id="logo" src="/images/Logo-IFRS.png"> *}
        <img id="logo" src="/images/Logo-Abrigo.png" />
    </a>
    {if isset($SESSAO['logado'])}
    <div class="parteBotao">
        <button id="botaoMenu">
            <img src="/images/Menu-Icon.svg" />
        </button>
    </div>
        <ul id="parteMenu" style="display: none;">
            {if isset($SESSAO['logado']) && ($SESSAO['logado'] == 'recepcao' || $SESSAO['logado'] == 'voluntario' || $SESSAO['logado'] == 'admin')   }
                <li><a href="/selecaoFoto">Foto <div class="menuIcon"><i class="fas fa-camera"></i></div></a></li>
                {* <li><a href="/admin/estatisticas/telaGeral">Contagem</a></li> *}
                <li><a href="/admin/abrigado/telaGeral/0">Abrigados <div class="menuIcon"><i class="fas fa-house-user"></i></div></a></li>
                <li><a href="/admin/voluntario/telaGeral">Voluntários <div class="menuIcon"><i class="fas fa-people-carry"></i></div></a></li>
                <li><a href="/admin/abrigado/banho">Banho <div class="menuIcon"><i class="fas fa-shower"></i></div></a></li>
            {/if}
         
            {if isset($SESSAO['logado']) && $SESSAO['logado'] == 'portaria'}
                <li><a href="/portaria">Registrar Entrada e Saída <div class="menuIcon"><i class="fas fa-door-open"></i></div></a></li>
                <li><a href="/admin/voluntario/telaGeral">Voluntários <div class="menuIcon"><i class="fas fa-people-carry"></i></div></a></li>

            {/if}
            {if isset($SESSAO['logado']) && $SESSAO['logado'] == 'roupas'}
                <li><a href="/admin/abrigado/roupas">Registrar retirada <div class="menuIcon"><i class="fas fa-tshirt"></i></div></a></li>
                <li><a href="/admin/doacao/roupas">Doação externa <div class="menuIcon"><i class="fas fa-hand-holding-heart"></i></div></a></li>
            {/if}
            {if isset($SESSAO['logado']) && $SESSAO['logado'] == 'higiene'}
                <li><a href="/admin/abrigado/higiene">Registrar retirada <div class="menuIcon"><i class="fas fa-toilet-paper"></i></div></a></li>
                <li><a href="/admin/abrigado/nucleoFamiliarHigiene">Retirada núcleo familiar <div class="menuIcon"><i class="fas fa-hand-holding-heart"></i></div></a></li>
                <li><a href="/admin/doacao/higiene">Doação externa <div class="menuIcon"><i class="fas fa-hand-holding-heart"></i></div></a></li>
                <li><a href="/admin/doacao/historicoHigiene">Doação externa(Histórico) <div class="menuIcon"><i class="fas fa-hand-holding-heart"></i></div></a></li>

            {/if}
            {if isset($SESSAO['logado']) && $SESSAO['logado'] == 'banho'}
                <li><a href="/admin/abrigado/banho">Banho <div class="menuIcon"><i class="fas fa-shower"></i></div></a></li>
                <li><a href="/admin/abrigado/banhoAdd">Adicionar na fila <div class="menuIcon"><i class="fas fa-hand-holding-heart"></i></div></a></li>
            {/if}
            {if isset($SESSAO['logado']) && ($SESSAO['logado'] == 'veterinaria' || $SESSAO['logado'] == 'admin' || $SESSAO['logado'] == 'voluntario')}
                <li><a href="/admin/pet/todos">Pets do abrigo <div class="menuIcon"><i class="fas fa-bone"></i></div></a></li>
                {*<li><a href="/admin/abrigado/pets">Tutores <div class="menuIcon"><i class="fas fa-users"></i></div></a></li>*}
            {/if}
            {if isset($SESSAO['logado']) && $SESSAO['logado'] == 'alimentacao'}
                <li><a href="/admin/abrigado/refeicao">Refeição abrigados <div class="menuIcon"><i class="fas fa-utensils"></i></div></a></li>
                {* <li><a href="/admin/voluntario/refeicao">Voluntários</a></li> *}
            {/if}
            {if isset($SESSAO['logado']) && $SESSAO['logado'] == 'enfermaria'}
                <li><a href="/admin/abrigado/enfermaria">Registros enfermaria <div class="menuIcon"><i class="fas fa-pills"></i></div></a></li>
            {/if}
            {if isset($SESSAO['logado']) && $SESSAO['logado'] == 'medicos'}
                <li><a href="/admin/abrigado/refeicao">Registros Médicos <div class="menuIcon"><i class="fas fa-notes-medical"></i></div></a></li>
                <li><a href="/admin/abrigado/laudos">Laudos <div class="menuIcon"><i class="fas fa-x-ray"></i></div></a></li>
            {/if}
            {if isset($SESSAO['logado']) && $SESSAO['logado'] == 'admin'}

                <li><a href="/admin/abrigado/nucleoFamiliar">Núcleo familiar <div class="menuIcon"><i class="fas fa-users"></i></div></a></li>


                <li><a href="/admin/doacaoRecebimento/pix">Doações PIX <div class="menuIcon"><i class="fas fa-hand-holding-usd"></i></div></a></li>
                <li><a href="/admin/compra/manter">Compras PIX <div class="menuIcon"><i class="fas fa-dollar-sign"></i></div></a></li>

                <li><a href="/cadastrosAdmin">Cadastros <div class="menuIcon"><i class="fas fa-laptop-house"></i></div></a></li>
                <li><a href="/admin/abrigado/tudo/0">CSVs <div class="menuIcon"><i class="fas fa-file-csv"></i></div></a></li>
            {/if}
            <li><a href="/chat">Converse com os dados <div class="menuIcon"><i class="fas fa-comments"></i></div></a></li>
            <li><a href="/admin/abrigado/estatisticas">Dados do Abrigo <div class="menuIcon"><i class="fas fa-database"></i></div></a></li>
            <li><a href="/login/logout">Logout <div class="menuIcon"><i class="fas fa-sign-out-alt"></i></div></a></li>
        </ul>
    {/if}
</header>
<div id="wrapperGeral">
{* {$MSG} *}