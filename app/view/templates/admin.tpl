<section class="container">
    <h2>{$TITLE}</h2>
    <p>{$DESCRIPTION}</h2>

    <h3> Admin </h3>
    <dl>
        <dt> <a href="/admin/papel/manter">Papeis</a></dt>
        <dd>Engloba os usuários e papeis do sistema utilize esse recurso para alterar as senhas. </dd>

        <dt> <a href="/admin/tipoRegistro/manter">Tipos de registros</a></dt>
        <dd>Os registros servem para fazer o log do abrigado. Se quiser ser uma função especifica cadastre como sys e fale com um desenvolvedor. </dd>

        <dt> <a href="/admin/situacao/manter">Situação dos abrigados</a></dt>
        <dd>A situação permite saber como está o Desabrigado em relação ao abrigo. </dd>


        <dt> <a href="/admin/sala/manter">Salas/quartos</a></dt>
        <dd>Cadastro e manutenção de salas/quartos onde os abrigados irão ficar dormindo. </dd>


        <dt> <a href="/admin/tipoPet/manter">Tipos de Pets</a></dt>
        <dd>Cadastre os pets além de gatos e cachorros. </dd>

        <dt> <a href="/admin/tipoResponsavel/manter">Tipos responsável</a></dt>  
        <dd>Veja os tipos de responsáveis padrões e adicione outros tipos para posterior classificação. </dd>
    
        <dt> <a href="/admin/tipoPessoa/manter">Tipos de pessoa</a></dt>

        <dt> <a href="/admin/tipoDoacao/manter">Tipos de Doação</a></dt>

        
    </dl> 
 </section>

{literal}
<style>

    dd{
        text-indent: 20pt;
    }

</style>
{/literal}