
        <table id="tabelaAbrigados">

            <colgroup>
                <col span="1" style="width: 4em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 30em;">
            </colgroup>

            <thead id="headTabela">
                <tr>
                    <th>ID</th>
                    <th>FOTO</th>
                    <th>NOME</th>
                    <th>IDADE</th>
                    <th>SALA</th>
                    <th>SITUAÇÃO</th>
                    <th>BAIRRO</th>
                    <th>CPF</th>
                    <th>RG</th>
                    <th>OBSERVAÇÕES</th>
                    <th>PETS</th>
                    {* 
                    deficiencia
                    mapeamento cidade e bairro
                    registro
                    botao saída definitiva
                    checklist de saída
                *}
                </tr>
            </thead>
            <tbody>
                {* admin/abrigado/editar/?{$abrigado->getID()} *}
                {foreach $abrigados as $abrigado}
                    <tr>
                        <td class="id">{$abrigado->getId()}</td>
                        <td class="foto">
                            {if $abrigado->getFoto()}
                                <a href="/admin/abrigado/getFotoModal/{$abrigado->getId()}" class="modalGeral"><img class="imagemTabela" src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" width="100%"></a>
                            {else}
                                Tirar foto
                            {/if}
                        </td>
                        <td class="nome">{$abrigado->getNomeCompleto()}</td>
                        <td class="idade">{$abrigado->getIdade()}</td>
                        <td class="sala">
                            {foreach $salas as $sala}
                                {if $sala->getId() == $abrigado->getSalaId()}{$sala->getBloco()}{$sala->getSala()}{/if}
                            {/foreach}
                        </td>
                        <td class="situacao">
                            {foreach $situacoes as $situacao}
                                {if $abrigado->getSituacaoId() == $situacao->getId()}{$situacao->getSituacao()}{{/if}}
                            {/foreach}
                        </td>
                        <td >{$abrigado->getBairro()}</td>
                        <td>{$abrigado->getCpf()}</td>
                        <td>{$abrigado->getDocumento()}</td>

                        <td class="observacao">{$abrigado->getObservacao()}</td>
                        <td class="observacao"></td>
                    </tr>
                {/foreach}

            </tbody>

        </table>