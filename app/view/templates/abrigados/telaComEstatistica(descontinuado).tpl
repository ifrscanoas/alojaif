
    <div id="containerGeral">
     <div id="containerMaior">
        <div class="containerDados" id="dadosSalas">
            <h5>SALAS</h5>
            <div class="containerSalas">
                <p>F2: <span id="F2"></span></p>
                <p>F3: <span id="F3"></span></p>
                <p>F4: <span id="F4"></span></p>
                <p>F5: <span id="F5"></span></p>
                <p>F6: <span id="F6"></span></p>
                <p>F7: <span id="F7"></span></p>
                <p>F8: <span id="F8"></span></p>
                <p>F9: <span id="F9"></span></p>
                <p>F10: <span id="F10"></span></p>
                <p>F11: <span id="F11"></span></p>
                <p>F101: <span id="F101"></span></p>
                <p>F102: <span id="F102"></span></p>
                <p>B: <span id="B"></span></p>
                <p>C: <span id="C"></span></p>
                <p>D1: <span id="D1"></span></p>
                <p>D2: <span id="D2"></span></p>
                <p>D3: <span id="D3"></span></p>
                <p>D4: <span id="D4"></span></p>
                <p>Caminhao: <span id="caminhao"></span></p>
                <p>Não informado: <span id="salaNaoInformada"></span></p>
            </div>
        </div>
        <div class="containerDados" id="dadosAbrigados">
            <h5>ABRIGADOS</h5>
            <h4 id="totalAbrigados"></h4>
            <p>Adultos: <span id="adultos"></span></p>
            <p>Adolescentes: <span id="adolescentes"></span></p>
            <p>Crianças: <span id="criancas"></span></p>
            <p>Idosos: <span id="idosos"></span></p>

            <div class="divider"></div>
            <p>Pessoas já abrigadas no campus: <span id="totalGeral"></spam>
            </p>
        </div>
        </div>
        <div id="containerOutros">
            <div class="containerDefault">
                <div class="containerDados" id="dadosDesocupacoes">
                    <h5>DESOCUPAÇÕES</h5>
                    <h5 id="totalDesocupacoes"></h5>
                </div>
                <div class="containerDados" id="dadosSaidaTemporaria">
                    <h5>SAÍDA TEMPORÁRIA</h5>
                    <h5 id="totalSaida"></h5>
                </div>
            </div>
            <div class="containerDefault">
                <div class="containerDados" id="dadosAmbulatorio">
                    <h5>AMBULATÓRIO</h5>
                    <h5 id="totalAmbulatorio"></h5>
                </div>
                <div class="containerDados" id="dadosTrabalho">
                    <h5>TRABALHO</h5>
                    <h5 id="totalTrabalho"></h5>
                </div>
            </div>
            <div class="containerDefault">
                <div class="containerDados" id="dadosNaoEncontrado">
                    <h5>NÃO LOCALIZADO</h5>
                    <h5 id="totalNaoLocalizado"></h5>
                </div>
            </div>
        </div>
        <table id="tabelaAbrigados">

            <colgroup>
                <col span="1" style="width: 4em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 30em;">
            </colgroup>

            <thead id="headTabela">
                <tr>
                    <th>ID</th>
                    <th>FOTO</th>
                    <th>NOME</th>
                    <th>IDADE</th>
                    <th>SALA</th>
                    <th>SITUAÇÃO</th>
                    <th>OBSERVAÇÕES IMPORTANTES</th>
                    {* 
                    deficiencia
                    mapeamento cidade e bairro
                    registro
                    botao saída definitiva
                    checklist de saída
                *}
                </tr>
            </thead>
            <tbody>
                {* admin/abrigado/editar/?{$abrigado->getID()} *}
                {foreach $abrigados as $abrigado}
                    <tr>
                        <td class="id">{$abrigado->getId()}</td>
                        <td class="foto">
                            {if $abrigado->getFoto()}
                                <a href="/admin/abrigado/getFotoModal/{$abrigado->getId()}" class="modalGeral"><img class="imagemTabela" src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" width="100%"></a>
                            {/if}
                        </td>
                        <td class="nome">{$abrigado->getNomeCompleto()}</td>
                        <td class="idade">{$abrigado->getIdade()}</td>
                        <td class="sala">
                            {foreach $salas as $sala}
                                {if $sala->getId() == $abrigado->getSalaId()}{$sala->getBloco()}{$sala->getSala()}{/if}
                            {/foreach}
                        </td>
                        <td class="situacao">
                                {foreach $situacoes as $situacao}
                                        {if $abrigado->getSituacaoId() == $situacao->getId()}{$situacao->getSituacao()}{{/if}}>
                                
                                {/foreach}
                        </td>
                        <td class="observacao">{$abrigado->getObservacao()}</td>
                    </tr>
                {/foreach}

            </tbody>

        </table>