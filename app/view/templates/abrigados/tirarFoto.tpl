{* {html_toogle name="mostrarDesocupou" value="true" label="Mostrar exodo"} *}
{* <hr> *}

    <div id="orientacoes">
        <h3>Instruções de uso:</h3>
        <ol>
            <li>
                Pesquise o nome do abrigado que você vai registrar a foto (o nome deve ser exatamento o mesmo)
            </li>
            <li>
                Fotografe ou troque a foto do abrigado como necessário
            </li>
            <li>
                Aguarde o envio da imagem e verifique se ela foi enviada reniciando a página
            </li>
        </ol>
        <h5>O registro da foto deve ser feito pelo celular!</h5>
    </div>

     <h5 style="text-align: center;">Pesquise aqui o nome da pessoa:</h5>
    <table id="tabelaAbrigados">

        <colgroup>
            <col span="1" style="width: calc(5%);">
            <col span="1" style="width: calc(20%);">
            <col span="1" style="width: calc(75%);">
        </colgroup>

        <thead>
            <tr>
                <th>ID</th>
                <th>FOTO</th>
                <th id="nomeColuna">NOME</th>
            </tr>
        </thead>
        <tbody>
            {* admin/abrigado/editar/?{$abrigado->getID()} *}
            {foreach $abrigados as $abrigado}
                <tr>
                    <td class="nome">
                        {$abrigado->getID()}
                    </td>
                    <td class="foto">
                        <div style="display: flex; align-items: center; justify-content: center;">
                            <div id="loading" class="loader" style="display: none;"></div>
                        </div>
                        {if !$abrigado->getFoto()}
                            <img id="foto{$abrigado->getId()}" width="100%" />
                            <div id="divFoto{$abrigado->getId()}">
                                <label class="control-label btn btn-primary enviaFotoBotao" for="fotoInput{$abrigado->getId()}">Enviar Foto <i class="fas fa-camera"></i></label>
                                <form id="dropzone{$abrigado->getId()}" enctype="multipart/form-data">
                                    <input type="file" id="fotoInput{$abrigado->getId()}" 
                                        class="inputFoto form-control" 
                                        data-id="{$abrigado->getId()}" name="foto"
                                        accept="image/jpeg" />
                                </form>
                            </div>
                        {else}
                            <img id="foto{$abrigado->getId()}" src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" width="100%">
                            <label class="control-label btn btn-warning" for="fotoInput{$abrigado->getId()}">Trocar Foto</label>
                            <form id="dropzone{$abrigado->getId()}" enctype="multipart/form-data">
                                <input type="file" id="fotoInput{$abrigado->getId()}" 
                                    class="inputFoto form-control" 
                                    data-id="{$abrigado->getId()}" 
                                    data-tem-foto="true" 
                                    name="foto"
                                    accept="image/jpeg" />
                            </form>
                        {/if}
                    </td>
                    <td class="nome">
                        {$abrigado->getNomeCompleto()}
                    </td>
                </tr>
            {/foreach}

        </tbody>
    </table>

    <h6>Não achou o abrigado? cadastre-o <a href="/admin/abrigado/criarNovo">aqui</a></h6>