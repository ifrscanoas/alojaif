<div id="content">
    <div id="loader" class="loading"></div>
    <div class="wrapper-table">
        <div id="divDeCima" class="sticky">
            {* {html_toogle name="apenasAbrigados" value="true" checked=$apenasAbrigados label="Mostrar apenas abrigados"}  *}
            <div>Filtro: 
                <select id="situacaoFilter" class="selectEstado todos" name="situacao_id">
                    <option class="todos" selected value="0">TODOS ABRIGADOS</option>
                    {* <option class="abrigadosGeral" selected value="20">ABRIGADOS (TODOS)</option> *}
                    {* <option class="desocupacoesGeral" value="21">DESOCUPAÇÕES (TODOS)</option> *}
                    {foreach $situacoes as $situacao}
                        {if $situacao->getSituacao() != 'DESOCUPOU' && $situacao->getSituacao() != 'ÓBITO' && $situacao->getSituacao() != 'MOVIDO' && $situacao->getSituacao() != 'TESTE'}
                            <option class="{$situacao->getCor()}" value="{$situacao->getId()}">
                                {$situacao->getSituacao()}</option>
                        {/if}
                    {/foreach}
                </select>
            </div>
            <div>Pesquisar: <input id="search"></input></div>
        </div>
        <table id="tabelaAbrigados" style="border: none !important;
        border-bottom: none !important;
        border-width: 0px !important;" class="loading">

            <colgroup>
                <col span="1" style="width: 4em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 5em;">
            </colgroup>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>FOTO</th>
                    <th>NOME</th>
                    <th>IDADE</th>
                    <th>SALA</th>
                    <th>SITUAÇÃO</th>
                    <th>OBSERVAÇÕES</th>
                    <th>AÇÕES</th>
                </tr>
            </thead>
            <tbody>
                {foreach $abrigados as $abrigado}
                    <tr>
                        <td class="id">{$abrigado->getId()}</td>
                        <td class="foto">
                            {if !$abrigado->getFoto()}
                                <a href="/admin/abrigado/tirarFoto"><button class="btn btn-secondary">Tirar Foto</button></a>
                            {else}
                                <a href="/admin/abrigado/getFotoModal/{$abrigado->getId()}" class="modalGeral">
                                    <img class="imagemTabela miniatura" src="/admin/abrigado/getMiniatura/{$abrigado->getId()}"
                                        width="100%">
                                </a>
                            {/if}
                        </td>
                        <td class="nome">
                            <a href="/admin/abrigado/ver/{$abrigado->getId()}/ajax" class="linkModal">
                                {$abrigado->getNomeCompleto()}
                            </a>
                        </td>
                        <td class="idade">{$abrigado->getIdade()}</td>
                        <td class="sala">
                            {foreach $salas as $sala}
                                {if $sala->getId() == $abrigado->getSalaId()}{$sala->getBloco()}{$sala->getSala()}{/if}
                            {/foreach}
                        </td>
                        <td class="situacao">
                            <select data-id="{$abrigado->getId()}"
                                class="selectEstado {$situacaoFragment->getCor($abrigado->getSituacaoId())}"
                                name="situacao_id">
                                {foreach $situacoes as $situacao}
                                    <option class="{$situacao->getCor()}" value="{$situacao->getId()}"
                                        {if $abrigado->getSituacaoId() == $situacao->getId()}selected{{/if}}>
                                        {$situacao->getSituacao()}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td class="observacao">
                            {$abrigado->getObservacao()}
                            {if empty($abrigado->getBairro())}
                                {if $abrigado->getObservacao()}
                                    <hr />
                                {/if}
                                Solicite o bairro para o Abrigado e preencha <a
                                    href="/admin/abrigado/editar/{$abrigado->getId()}" class="linkModal">aqui</a>
                            {/if}
                        </td>
                        <td class="sala">
                            {if $SESSAO['logado'] == 'admin'}
                                <a href="/admin/abrigado/editar/{$abrigado->getId()}"
                                    class="linkModal btn btn-secondary">Editar</a>
                            {/if}
                            <button class="btn btn-secondary addRegistro" data-id="{$abrigado->getId()}"> Add
                                Registro</button>
                            <button class="btn btn-secondary addVeiculo" data-id="{$abrigado->getId()}"> Add
                                Veículo</button>
                            <button class="btn btn-secondary addPulseira" data-id="{$abrigado->getId()}"> Registrar entrega
                                de pulseira (<span
                                    id="qtdPul{$abrigado->getId()}">{$abrigado->getQuantidadePulseiras($registrosPulseira)}</span>)</button>
                            {if $SESSAO['logado'] == 'admin'}
                                <button class="btn btn-primary addResponsavel" data-id="{$abrigado->getId()}">
                                    Add familiar
                                </button>
                            {/if}
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>

    {if $SESSAO['logado'] == 'admin'}
        <a href="/admin/abrigado/criarNovo" class="btn btn-primary">Registrar novo abrigado</a>
    {/if}
</div>