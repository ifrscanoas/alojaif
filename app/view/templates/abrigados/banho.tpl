<div class="row">
<div class="col">
<a  class="btn btn-info" style="width: 100px; margin: 1em" href="/admin/abrigado/banhoAdd">Adicionar ao banho</a>  


</div>

<div class="col">
    <button id="button" class="btn btn-info" style="width: 100px; margin: 1em">Imprimir</button>  
</div>

</div>

<table id="tabelaAbrigados" style="border:2px solid black;width:90%">

    <colgroup>
        <col span="1" style="width: calc(5%);">
        <col span="1" style="width: calc(20%);">
        <col span="1" style="width: calc(70%);">
        <col span="1" style="width: calc(5%);">
    </colgroup>

    <thead>
        <tr style="border:2px solid black">
            <th>ID</th>
            <th>FOTO</th>
            <th id="nomeColuna">NOME</th>
            <th >Ações</th>

        </tr>
    </thead>
    <tbody>
        {* admin/abrigado/editar/?{$abrigado->getID()} *}
        {foreach $abrigados as $abrigado}
            <tr  style="border:2px solid black">
                <td class="nome">
                    {$abrigado->getID()}
                </td>
                <td class="foto">
                    <img id="foto{$abrigado->getId()}" src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" class="miniatura" />

                </td>
                <td >
                    {$abrigado->getNomeCompleto()}
                </td>
                <td >
                    
                    <a class="btn btn-success" href="/admin/abrigado/baixaBanho/{$abrigado->getID()}">Retorno do banho</a>
                    
                </td>
            </tr>
        {/foreach}

    </tbody>
</table>