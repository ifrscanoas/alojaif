{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Abrigado</legend>
    <input type="hidden" id="id" name="id" class=" form-control" required  value="{$abrigado->getId()}" />
    <div class="row">
        <div class="col-4 text-center">
                {if $abrigado->getFoto()}
                    <a href="/admin/abrigado/getFotoModal/{$abrigado->getId()}" ><img src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" width="20%"></a>
                {/if}
                <div id="dropzone">
                    <input type="file" id="foto" name="foto" class=" form-control" accept="image/jpeg" />
                </div>
        </div>
        <div class="col-8">
            <div class="form-group row">
                <label class="control-label col-sm-4" for="nomeCompleto">Nome completo</label>
                <div class="col-sm-8">
                    <input type="text" id="nomeCompleto" name="nomeCompleto" class=" form-control" required  value="{$abrigado->getNomeCompleto()}" 
                        {if $SESSAO["logado"] != 'admin' && $abrigado->getId()}
                            readonly
                        {/if}
                    />
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-4" for="situacaoId">Situação</label>
                <div class="col-sm-8">
                    <select id="situacaoId" name="situacaoId" 
                    {if $SESSAO["logado"] != 'admin' && $abrigado->getId()}
                        readonly="readonly" disabled="disabled"                    
                    {/if}
                    class="form-control">
                        {html_options options=$listaSituacao selected=$abrigado->getSituacaoId()}
                    </select>
                    {if $SESSAO["logado"] == 'admin'}
                        <a href="/admin/situacao/criarNovo?return=admin/Abrigado" class="addData">[+] Situação</a>
                    {/if}
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-4" for="blocoId">Sala</label>
                <div class="col-sm-8">
                    <select id="salaId" name="salaId" class="form-control">
                        {html_options options=$listaSala selected=$abrigado->getSalaId()}
                    </select>
                    <a href="/admin/sala/criarNovo" class="addData">[+] Sala</a>
                </div>
            </div>        
        </div>
</div>      


        <div class="row">
            <div class="col">
                <label class="control-label col-sm-4" for="cpf">CPF</label>
                <input type="text" id="cpf" name="cpf" class=" form-control"  value="{$abrigado->getCpf()}" />
            </div>     
            <div class="col">
            <label class="control-label col-sm-4" for="documento">RG</label>
                <input type="text" id="documento" name="documento" class=" form-control"  value="{$abrigado->getDocumento()}" />
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-4" for="foto">Observações</label>
            <div class="col-sm-8">
                <textarea name="observacao" class=" form-control">{$abrigado->getObservacao()}</textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-4" for="dataNascimento">Data nascimento</label>
            <div class="col-sm-8">
                 <input type="date" id="dataNascimento" name="dataNascimento" class=" form-control"  value="{$abrigado->getDataNascimento()}" />
            </div>
        </div>

        <div class="form-group row">
            <label class="control-label col-sm-4" for="endereco">Endereço</label>
            <div class="col-sm-8">
                 <input type="text" id="endereco" name="endereco" class=" form-control"  value="{$abrigado->getEndereco()}" />
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-4" for="bairro">Bairro</label>
            <div class="col-sm-8">
                 <input type="text" id="bairro" name="bairro" class=" form-control" value="{$abrigado->getBairro()}" />
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-4" for="cidade">Cidade</label>
            <div class="col-sm-8">
                <input type="text" id="cidade" name="cidade" class=" form-control" value="" readonly/>
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-4" for="telefone">Telefone</label>
            <div class="col-sm-8">
                 <input type="text" id="telefone" name="telefone" class=" form-control" value="{$abrigado->getTelefone()}" />
            </div>
        </div>
    {if !empty($abrigado->getDataSaida())}
        <div class="form-group row">
            <label class="control-label col-sm-4" for="dataSaida">Data saída</label>
            <div class="col-sm-8">
                <input type="text" id="dataSaida" name="dataSaida" class=" form-control" value="{$abrigado->getDataSaida()}" />
            </div>
        </div>
    {/if}


    {if isset($responsaveis) && sizeof($responsaveis) > 0}
        <h2>Responsáveis</h2>
        <table border="1" >

            <colgroup>
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 30em;">
            </colgroup>
    
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Foto</th>
                    <th>Tipo Responsável</th>
                    <th>Nome</th>
                {if $SESSAO["logado"] == 'admin'}
                    <th>Ações</th>
                {/if}
                </tr>
            </thead>
            <tbody>
            {foreach $responsaveis as $responsavel}
                <tr>
                    <td>
                        {$responsavel->getAbrigado()->getId()}
                    </td>
                    <td>
                    {if $responsavel->getAbrigado()->getFoto()}
                        <a href="/admin/abrigado/getFoto/{$responsavel->getAbrigado()->getId()}"><img src="/admin/abrigado/getMiniatura/{$responsavel->getAbrigado()->getId()}" width="20%"></a>
                    {/if}
                    </td>
                    <td>
                        {$responsavel->getTipoResponsavel($tiposResponsaveis)}
                    </td>
                    <td>
                        {$responsavel->getAbrigado()->getNomeCompleto()}
                    </td>
                          
                    {if $SESSAO["logado"] == 'admin'}
                    <td>
                        <a href="/admin/responsavel/deletarFim/{$responsavel->getID()}">[x] </a>
                    </td>
                    {/if}
                </tr>
            {/foreach}
            </tbody>
            </table>
            <hr />
        {/if}

        {if isset($registros)  && sizeof($registros) > 0}
            <table border="1" id="tabelaRegistro">

            <colgroup>
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 30em;">
            </colgroup>
    
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Observações</th>
                    <th>Data</th>
                {if $SESSAO["logado"] == 'admin'}
                    <th>Ações</th>
                {/if}
                </tr>
            </thead>
            <tbody>
            {foreach $registros as $registro}
                <tr>
                    <td>
                        {$registro->getTipoRegistro()->getTipo()}
                    </td>
                    <td>
                        {$registro->getObservacao()}
                    </td>
                    <td>
                        {$registro->getDataRegistroFormatada()}
                    </td>
                    {if $SESSAO["logado"] == 'admin'}
                    <td>
                        <a href="/admin/registro/deletarFim/{$registro->getID()}">[x]</a>
                    </td>
                    {/if}
                </tr>
            {/foreach}
            </tbody>
            </table>
        {/if}
        {if $abrigado->getId()}
        <a href="/admin/pet/criarNovoPorAbrigado/{$abrigado->getId()}" >[+] Adicionar pet</a>
        {/if}
        {if isset($pets) && sizeof($pets) > 0 }
            <h3>Pets</h3>
            <table border="1" id="tabelaPets">

            <colgroup>
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 30em;">
            </colgroup>
    
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Nome</th>
                    <th>Foto</th>
                </tr>
            </thead>
            <tbody>
            {foreach $pets as $pet}
                <tr>
                    <td>
                        {$pet->getTipo()}
                    </td>
                    <td>
                        {$pet->getNome()}
                    </td>

                    <td>
                    {if $pet->getFoto()}
                        <img src="/admin/pet/getFoto/{$pet->getId()}" width="20%">
                    {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
            </table>
        {/if}
</fieldset>