<div id="content">
    <div class="wrapper-table">
        <table id="tabelaAbrigados">
            <thead id="headTabela">
                <tr>
                    <th>ID</th>
                    <th>FOTO</th>
                    <th>NOME</th>
                    <th>IDADE</th>
                    <th>SALA</th>
                    <th>SITUAÇÃO</th>
                    <th>AÇÕES</th>
                    <th>OBSERVAÇÕES</th>
                </tr>
            </thead>
            <tbody>
                {* admin/abrigado/editar/?{$abrigado->getID()} *}
                {foreach $abrigados as $abrigado}
                    <tr>
                        <td class="id">{$abrigado->getId()}</td>
                        <td class="foto">
                            {if !$abrigado->getFoto()}
                                <a href="/admin/abrigado/tirarFoto"><button class="btn btn-secondary">Tirar Foto</button></a>
                            {else}
                                <a href="/admin/abrigado/getFotoModal/{$abrigado->getId()}" class="modalGeral" target="_blank">
                                    <img class="imagemTabela miniatura" src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" width="100%">
                                </a>
                            {/if}
                        </td>
                        <td class="nome">
                            <a href="/admin/abrigado/editar/{$abrigado->getId()}"
                                class="linkModal">{$abrigado->getNomeCompleto()}</a></td>
                        <td class="idade">{$abrigado->getIdade()}</td>
                        <td class="sala">
                            {foreach $salas as $sala}
                                {if $sala->getId() == $abrigado->getSalaId()}{$sala->getBloco()}{$sala->getSala()}{/if}
                            {/foreach}
                        </td>
                        <td class="situacao">
                                {foreach $situacoes as $situacao}
                                    {if $abrigado->getSituacaoId() == $situacao->getId()}
                                        <span class="badge" style="background-color: {$situacao->getCor()};" >
                                        {$situacao->getSituacao()}
                                        </span>
                                    {/if}
                                {/foreach}
                        </td>
                        <td class="acoes">
                            {if isset($ACOES)}
                                {include file=$ACOES} 
                            {/if}
                        </td>
                        <td class="observacao">
                            {if isset($OBSERVACOES)}
                                {include file=$OBSERVACOES} 
                                <hr/>
                            {/if}
                            {$abrigado->getObservacao()}
                            {if empty($abrigado->getBairro())}
                            <hr />
                                Solicite o bairro para o Abrigado e preencha   <a href="/admin/abrigado/editar/{$abrigado->getId()}"
                                class="linkModal">Aqui</a>
                            {/if}
                        </td>
                    </tr>
                {/foreach}

            </tbody>

        </table>

        
    </div>

{if $SESSAO['logado'] == 'admin'}
    <a href="/admin/abrigado/criarNovo" class="btn btn-primary">Registrar novo abrigado</a>
{/if}
</div>
