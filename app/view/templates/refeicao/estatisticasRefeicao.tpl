<div id="content">
    <h3 style="text-align: center;">Refeições</h3>
    <table id="almoco" class="tabelaPadrao">
        <col span="1" style="width: 33.33%;">
        <col span="1" style="width: 33.33%;">
        <col span="1" style="width: 33.33%;">
        <thead>
            <tr>
                <th>
                    Dia
                </th>
                <th>
                    Almoço
                </th>
                <th>
                    Janta
                </th>
            </tr>
        </thead>
        <tbody>
            {foreach $dias as $key => $dia}
                <td>
                    <h4>{$key}</h4>
                </td>
                <td>
                    <h4>{$dia['almoco']}</h4>
                </td>
                <td>
                    <h4>{$dia['janta']}</h4>
                </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>