<header id="header">
    <a href="/admin/abrigado/telaGeral/0" class="parteLogo">
        {* <img id="logo" src="/images/Logo-IFRS.png"> *}
        <img id="logo" src="/images/Logo-Abrigo.png"></img>
    </a>
    <div class="parteBotao">
        <button id="botaoMenu">
            <img src="/images/Menu-Icon.svg" />
        </button>
    </div>
        <ul id="parteMenu" style="display: none;">
            <li><a href="/public/pessoas">Pessoas <div class="menuIcon"><i class="fas fa-users"></i></div></a></li>
            <li><a href="/public/pets">Pets <div class="menuIcon"><i class="fas fa-paw"></i></div></a></li>
            <li><a href="/public/doacao">Doações <div class="menuIcon"><i class="fas fa-hand-holding-heart"></i></div></a></li>
            <li><a href="/public/creditos">Créditos <div class="menuIcon"><i class="fas fa-stream"></i></div></a></li>
        </ul>
  
</header>
<div id="wrapperGeral">
{* {$MSG} *}