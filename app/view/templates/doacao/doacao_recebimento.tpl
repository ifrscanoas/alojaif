{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Doação recebimento</legend>
  
        <div class="form-group">
            <label class="control-label col-sm-2" for="dataRecebimento">Data recebimento</label>
            <div class="col-sm-8">
                 <input type="date" id="dataRecebimento" name="dataRecebimento" class=" form-control" required  value="{date('Y-m-d')}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="valor">Valor</label>
            <div class="col-sm-8">
                 <input type="text" id="valor" name="valor" class="validaFloat form-control" required  value="" />
            </div>
        </div>
  
</fieldset>

