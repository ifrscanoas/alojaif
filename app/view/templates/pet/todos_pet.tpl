<a href="/admin/pet/criarNovo" class="btn btn-primary">Adicionar um pet</a>
<a href="/admin/pet/nucleoFamiliar" class="btn btn-secondary">Nucleo Familiar</a>


<table border="1" id="tabelaPets">

<colgroup>
    <col span="1" style="width: 10em;">
    <col span="1" style="width: 20em;">
    <col span="1" style="width: 50em;">
    <col span="1" style="width: 10em;">

</colgroup>

<thead>
    <tr>
        <th>Foto</th>
        <th>Tipo</th>
        <th>Descrição</th>
        <th>Tutor</th>
    </tr>
</thead>
<tbody>
{foreach $pets as $pet}
    <tr>
        <td>
        {if $pet->getFoto()}
            <a href="/admin/pet/getFoto/{$pet->getId()}" target="_blank"><img src="/admin/pet/getMiniatura/{$pet->getId()}" class="miniatura"></a>
        {/if}
        <a href="/admin/pet/editar/{$pet->getId()}" class="btn btn-primary">Editar</a>
        <a href="/admin/pet/deletarFim/{$pet->getId()}" class="btn btn-danger">Excluir</a>
        </td>
        <td>
            {$tipos[$pet->getTipoPetId()]->tipo|default:'SEM ESPÉCIE'}
        </td>        
        <td>
            ID: <a href="/admin/pet/editar/{$pet->getId()}" class="linkModal">{$pet->getId()}</a> <br />
            Nome:  {$pet->getNome()} <br />
            Sexo:  {$pet->getSexo()}<br />
            Descrição: {$pet->getNome()}
            <p class="text-right"> <a href="/admin/pet/ver/{$pet->getId()}"> Ver mais sobre esse pet</a> </p>
        </td>
        <td class="text-center">
            <a href="/admin/abrigado/getFotoModal/{$pet->getAbrigadoId()}" class="modalGeral">
                <img src="/admin/abrigado/getMiniatura/{$pet->getAbrigadoId()}" class="miniatura">
            </a> 
            <br />
            <a href="/admin/abrigado/ver/{$pet->getAbrigadoId()}" >
                {$pet->getAbrigadoId()}
            </a> 
        </td>
    </tr>
{/foreach}
</tbody>
</table>