<tr>
    <td>
        {if $pet->getFoto()}
            <a href="/admin/pet/getFoto/{$pet->getId()}" target="_blank">
                <img src="{$BASE_URL}/admin/pet/getMiniatura/{$pet->getId()}" 
                    class="fotoTabela miniatura"
                > </a>
        {/if}
    </td>
    <td>
        {$pet->getTipo($tipos)}
    </td>
    <td>
        {$pet->getNome()}
    </td>
</tr>