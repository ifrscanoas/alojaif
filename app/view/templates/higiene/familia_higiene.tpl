<h2>{$TITLE}</h2>
<p>{$DESCRIPTION}</p>

{literal}
    <style>
ol li{
    margin-left: 100px;
}

ol{
    list-style: decimal;
}

</style>
{/literal}

<ol>
{foreach $familias as $familia}
    <li> Grupo familiar  <button class="btn btn-primary">[ + ] Kit Limpeza</button>
    <div >
        
    </div>
    <table>

        <colgroup>
            <col span="1" style="width: 4em;">
            <col span="1" style="width: 7em;">
            <col span="1" style="width: 30em;">
            <col span="1" style="width: 7em;">            
            <col span="1" style="width: 20em;">
        </colgroup>

        <thead id="headTabela">
            <tr>
                <th>ID</th>
                <th>FOTO</th>
                <th>NOME</th>
                <th>IDADE</th>
                <th>SALA</th>
                <th>BAIRRO</th>
                {* 
                    deficiencia
                    mapeamento cidade e bairro
                    registro
                    botao saída definitiva
                    checklist de saída
                *}
            </tr>
        </thead>
        <tbody>
            {* admin/abrigado/editar/?{$abrigado->getID()} *}
            {foreach $familia as $abrigado}
                <tr>
                    <td class="id">
                        {$abrigado->getId()}
                    </td>
                    <td class="foto">
                        {if $abrigado->getFoto()}
                            <a href="/admin/abrigado/getFotoModal/{$abrigado->getId()}" class="modalGeral">
                                <img class="imagemTabela miniatura"
                                    src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" 
                                    width="100%">
                            </a>
                        {/if}
                    </td>
                    <td class="nome">
                        <a href="/admin/abrigado/ver/{$abrigado->getId()}/ajax" class="modalGeral">
                        {$abrigado->getNomeCompleto()}
                        </a>                        
                    </td>
                    <td class="idade">{$abrigado->getIdade()}</td>
                    <td class="sala">
                        {$salas[$abrigado->getSalaId()]->bloco}{$salas[$abrigado->getSalaId()]->sala}
                    </td>
                    <td>{$abrigado->getBairro()}</td>

                  
                </tr>
            {/foreach}

        </tbody>

    </table>

    <hr style="border: 2px solid green"/>
</li>
{/foreach}
</ol>

<h2>Sem familia</h2>
<table>

        <colgroup>
            <col span="1" style="width: 4em;">
            <col span="1" style="width: 7em;">
            <col span="1" style="width: 30em;">
            <col span="1" style="width: 7em;">
            <col span="1" style="width: 7em;">
            
            <col span="1" style="width: 30em;">
            <col span="1" style="width: 20em;">
        </colgroup>

        <thead id="headTabela">
            <tr>
                <th>ID</th>
                <th>FOTO</th>
                <th>NOME</th>
                <th>IDADE</th>
                <th>SALA</th>
                <th>Bairro</th>
                <th>Função</th>
                {* 
                    deficiencia
                    mapeamento cidade e bairro
                    registro
                    botao saída definitiva
                    checklist de saída
                *}
            </tr>
        </thead>
        <tbody>
            {* admin/abrigado/editar/?{$abrigado->getID()} *}
            {foreach $semFamilia as $abrigado}
                <tr>
                    <td class="id">
                        {$abrigado->getId()}
                    </td>
                    <td class="foto">
                        {if $abrigado->getFoto()}
                            <a href="/admin/abrigado/getFotoModal/{$abrigado->getId()}" class="modalGeral">
                                <img class="imagemTabela miniatura"
                                    src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" 
                                    width="100%">
                            </a>
                        {/if}
                    </td>
                    <td class="nome">
                        <a href="/admin/abrigado/ver/{$abrigado->getId()}/ajax" class="modalGeral">
                        {$abrigado->getNomeCompleto()}
                        </a>                        
                    </td>
                    <td class="idade">{$abrigado->getIdade()}</td>
                    <td class="sala">
                        {$salas[$abrigado->getSalaId()]->bloco}{$salas[$abrigado->getSalaId()]->sala}
                    </td>
                    <td>{$abrigado->getBairro()}</td>

                    <td>
                        <button class="btn btn-success">Adicionar familiar</button>
                    </td>
                </tr>
            {/foreach}

        </tbody>

    </table>