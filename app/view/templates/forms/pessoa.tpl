{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Pessoa</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$pessoa->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="tipoPessoaId">Tipo pessoa id</label>
            <div class="col-sm-8">
                 <select id="tipoPessoaId" name="tipoPessoaId" class="form-control">
    		{html_options options=$listaTipoPessoa selected=$pessoa->getTipoPessoaId()}
             </select>
<a href="/admin/tipoPessoa/criarNovo?return=admin/Pessoa" class="addData">[+] Tipo pessoa</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="cpf">Cpf</label>
            <div class="col-sm-8">
                 <input type="text" id="cpf" name="cpf" class=" form-control"  value="{$pessoa->getCpf()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="observacao">Observação</label>
            <div class="col-sm-8">
                 <input type="text" id="observacao" name="observacao" class=" form-control"  value="{$pessoa->getObservacao()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="sys">Sys</label>
            <div class="col-sm-8">
                 <input type="text" id="sys" name="sys" class=" form-control"  value="{$pessoa->getSys()}" />
            </div>
        </div>
</fieldset>

