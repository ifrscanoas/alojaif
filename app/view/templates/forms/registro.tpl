{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Registro saída temporária</legend>
             <input type="hidden" id="abrigadoId" name="abrigadoId" class=" form-control" required  value="{$registro->getAbrigadoId()}" />
        <div class="form-group" style="visibility: hidden;">
            <label class="control-label col-sm-2" for="tipoRegistroId">Tipo de saída</label>
            <div class="col-sm-8">
                 <select id="tipoRegistroId" name="tipoRegistroId" class="form-control">
    		{html_options options=$listaTipoRegistro selected=$registro->getTipoRegistroId()}
             </select>
<a href="/admin/tipoRegistro/criarNovo?return=admin/Registro" class="addData">[+] Tipo registro</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="observacao">Observação</label>
            <div class="col-sm-8">
                 <textarea id="observacao" name="observacao" class=" form-control">{$registro->getObservacao()}</textarea>
            </div>
        </div>

</fieldset>

