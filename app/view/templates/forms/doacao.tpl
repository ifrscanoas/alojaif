{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Doação</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$doacao->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="pessoaId">Pessoa id</label>
            <div class="col-sm-8">
                 <select id="pessoaId" name="pessoaId" class="form-control">
    		{html_options options=$listaPessoa selected=$doacao->getPessoaId()}
             </select>
<a href="/admin/pessoa/criarNovo?return=admin/Doacao" class="addData">[+] Pessoa</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="tipoRegistroId">Tipo registro id</label>
            <div class="col-sm-8">
                 <select id="tipoRegistroId" name="tipoRegistroId" class="form-control">
    		{html_options options=$listaTipoRegistro selected=$doacao->getTipoRegistroId()}
             </select>
<a href="/admin/tipoRegistro/criarNovo?return=admin/Doacao" class="addData">[+] Tipo registro</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="dataRegistro">Data registro</label>
            <div class="col-sm-8">
                 <input type="text" id="dataRegistro" name="dataRegistro" class=" form-control"  value="{$doacao->getDataRegistro()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="observacao">Observação</label>
            <div class="col-sm-8">
                 <input type="text" id="observacao" name="observacao" class=" form-control"  value="{$doacao->getObservacao()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="quantidade">Quantidade</label>
            <div class="col-sm-8">
                 <input type="text" id="quantidade" name="quantidade" class=" form-control"  value="{$doacao->getQuantidade()}" />
            </div>
        </div>
</fieldset>

