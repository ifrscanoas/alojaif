{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Papel</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$papel->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario">Usuário</label>
            <div class="col-sm-8">
                 <input type="text" id="usuario" name="usuario" class=" form-control"  value="{$papel->getUsuario()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="senha">Senha</label>
            <div class="col-sm-8">
                 <input type="text" id="senha" name="senha" class=" form-control"  value="{$papel->getSenha()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="nomePapel">Nome papel</label>
            <div class="col-sm-8">
                 <input type="text" id="nomePapel" name="nomePapel" class=" form-control"  value="{$papel->getNomePapel()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="rotaBase">Rota base</label>
            <div class="col-sm-8">
                 <input type="text" id="rotaBase" name="rotaBase" class=" form-control"  value="{$papel->getRotaBase()}" />
            </div>
        </div>
</fieldset>

