{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Tipo doação</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$tipoDoacao->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="tipoDoacao">Tipo doação</label>
            <div class="col-sm-8">
                 <input type="text" id="tipoDoacao" name="tipoDoacao" class=" form-control"  value="{$tipoDoacao->getTipoDoacao()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="cor">Cor</label>
            <div class="col-sm-8">
                 <input type="text" id="cor" name="cor" class=" form-control"  value="{$tipoDoacao->getCor()}" />
            </div>
        </div>
</fieldset>

