{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Voluntario</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$voluntario->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="nomeCompleto">Nome completo</label>
            <div class="col-sm-8">
                 <input type="text" id="nomeCompleto" name="nomeCompleto" class=" form-control"  value="{$voluntario->getNomeCompleto()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="cpf">Cpf</label>
            <div class="col-sm-8">
                 <input type="text" id="cpf" name="cpf" class=" form-control"  value="{$voluntario->getCpf()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="telefone">Telefone</label>
            <div class="col-sm-8">
                 <input type="text" id="telefone" name="telefone" class=" form-control"  value="{$voluntario->getTelefone()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="foto">Foto</label>
            <div class="col-sm-8">
                {if $voluntario->getFoto()}
                    <img src="/admin/voluntario/getMiniatura/{$voluntario->getId()}" width="20%">
                {/if}
                <div id="dropzone">
                    <input type="file" id="foto" name="foto" class=" form-control" accept="image/jpeg" />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="tipo">Tipo</label>
            <select id="tipo" name="tipoVoluntarioId" class="form-control">
    		    {html_options options=$listaTipos selected=$voluntario->getTipoVoluntarioId()}
            </select>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="obs">Observações</label>
            <div class="col-sm-8">
                <textarea id="obs" name="obs" class=" form-control">{$voluntario->getObs()}</textarea>
            </div>
        </div>
</fieldset>

