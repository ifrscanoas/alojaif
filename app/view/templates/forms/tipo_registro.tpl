{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Tipo registro</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$tipoRegistro->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="tipo">Tipo</label>
            <div class="col-sm-8">
                 <input type="text" id="tipo" name="tipo" class=" form-control"  value="{$tipoRegistro->getTipo()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="sys">Sys</label>
            <div class="col-sm-8">
                 <input type="checkbox" id="sys" name="sys" class=" form-control"  value="true" {if $tipoRegistro->getSys()} checked="checked"{/if} />
            </div>
        </div>
</fieldset>

