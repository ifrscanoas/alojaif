{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Doação recebimento</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$doacaoRecebimento->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="pessoaId">Pessoa id</label>
            <div class="col-sm-8">
                 <select id="pessoaId" name="pessoaId" class="form-control">
    		{html_options options=$listaPessoa selected=$doacaoRecebimento->getPessoaId()}
             </select>
<a href="/admin/pessoa/criarNovo?return=admin/DoacaoRecebimento" class="addData">[+] Pessoa</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="tipoDoacaoId">Tipo doação id</label>
            <div class="col-sm-8">
                 <select id="tipoDoacaoId" name="tipoDoacaoId" class="form-control">
    		{html_options options=$listaTipoDoacao selected=$doacaoRecebimento->getTipoDoacaoId()}
             </select>
<a href="/admin/tipoDoacao/criarNovo?return=admin/DoacaoRecebimento" class="addData">[+] Tipo doação</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="dataRecebimento">Data recebimento</label>
            <div class="col-sm-8">
                 <input type="text" id="dataRecebimento" name="dataRecebimento" class=" form-control" required  value="{$doacaoRecebimento->getDataRecebimento()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="valor">Valor</label>
            <div class="col-sm-8">
                 <input type="text" id="valor" name="valor" class="validaFloat form-control" required  value="{$doacaoRecebimento->getValor()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="quantidade">Quantidade</label>
            <div class="col-sm-8">
                 <input type="text" id="quantidade" name="quantidade" class="validaInteiro form-control" required  value="{$doacaoRecebimento->getQuantidade()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="obs">Obs</label>
            <div class="col-sm-8">
                 <input type="text" id="obs" name="obs" class=" form-control"  value="{$doacaoRecebimento->getObs()}" />
            </div>
        </div>
</fieldset>

