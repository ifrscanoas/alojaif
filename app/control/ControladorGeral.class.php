<?php


/**
 * Classe principal do sistema responsável por criar a interface padrão assim como verificar 
 * a sessão e permissões do usuário.
 * 
 * @author Marcio Bigolin - <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class ControladorGeral extends AbstractController
{

    public function __construct()
    {

        $this->view = new VisualizadorGeral();
        if ($this->verificaLogado() == false) {
            $_SESSION['principal'] = '#';

            $this->redirect("/login");
        }
        $GLOBALS['ERROS'] = []; #TODO verificar pois esta vindo lixo nessa variável de algumas demandas
        $this->pwa();
        $this->view->addJS('geral');
    }

    private function verificaLogado(){
        if(isset($_SESSION['logado']) && $_SESSION['logado'] == true){
            return true;
        }
        return false;
    }

    private function pwa()
    {
        $this->view->addMetaTag('manifest', 'manifest.webmanifest');
    }


    public function privacidade()
    {
        $this->view->setTitle('Políticas de Privacidade');
        $this->view->addTemplate('privacidade');
    }

    public function index()
    {
       //$this->view->setTitle('Abrigo IFRS');  
       //Fazer uns dados
        $this->redirect('/admin/abrigado/telaGeral/0');

    }


    public function cadastrosAdmin(){
        $this->view->setTitle('Manutenção de dados que só o Admin tem');
        $this->view->setDescription('Nessa página está disponível os cadastros de tipos, papeis e configurações que cabem apenas ao administrador');

        $this->view->addTemplate('admin');
    }
    


    public function selecaoFoto()
    {
        $this->view->setTitle('Selecionar foto');
        $this->view->addTemplate('foto/selecaoFoto');
        $this->view->addCSS('selecaoFoto');
    }
    
    public function paginaNaoEncontrada()
    {
        $this->view->setTitle('Pagina não encontrada'); 

    }

    public function portaria()
    {
        $this->view->setTitle('Controle Pessoas ');

        $this->view->attValue('pessoas', Abrigado::getAll('situacao_id NOT IN (3,7,8,10)', 'nome_completo ASC'));
        $this->view->addTemplate('portaria/pessoas');
        $this->view->addCSS('abrigados');
        $this->view->addCSS('situacoes');
        $this->view->addCSS('portaria');

        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaAbrigados');
        $dataTable->addConf("bPaginate", false);
        $this->view->addComponente($dataTable);

        $this->view->attValue('salas', Sala::getMap());
        $this->view->attValue('situacaoFragment', new SituacaoFragment(Situacao::getMap()));

        $this->view->addJS('pluginAcentos');

        $this->view->addJS('portaria');

    }


    public function importarCSV(){
        if (($handle = fopen(CACHE . 'abrigados2.csv', "r")) !== FALSE) {
            fgetcsv($handle, 10000, ";"); //descarta cabeçalho
            dbd();   
            while (($linha = fgetcsv($handle, 10000, ";")) !== FALSE) {
                
                ds($linha);
              
                $abrigado = new Abrigado();
                $abrigado->setId($linha[1]);
                $abrigado->setNomeCompleto($linha[0]);
                $abrigado->setDocumento($linha[2]);
                if(empty($linha[3])){
                    $abrigado->ignoreField('dataNascimento');
                }else{
                    $abrigado->setDataNascimento($linha[3]);
                }
                $abrigado->setIdadeOld($linha[4]);
                $abrigado->setSalaId($this->localizarSala($linha[5], $linha[6]));
                $abrigado->setSituacaoId($this->localizaSituacao($linha[7]));

                if(empty($linha[8])){
                    $abrigado->ignoreField('dataSaida');
                }else{
                    $abrigado->setDataSaida($linha[8]);
                }
                $abrigado->setBairro($linha[9]);
                
                
                ds($abrigado); 
                $abrigado->removeIgnored('id'); 
                $abrigado->create();  
                if($linha[14] == 'SIM'){
                    $registro = new Registro();
                    $registro->setAbrigadoId($abrigado->getID());
                    $registro->setTipoRegistroId(9);
                    $registro->save();
                }

                if(!empty($linha[11])){
                    for($i = 0; $i < $linha[11]; $i++){
                        $pet = new Pet();
                        $pet->setAbrigadoId($abrigado->getID());
                        $pet->setTipo($linha[10]);
                        $pet->save();
                    }
                }

            }
            fclose($handle);
        }
    }

    public function chat()
    {
        $this->view->setTitle('Converse com os dados');

        $this->view->addJS('chat');
        $this->view->addCss('chat');
        $this->view->addTemplate('chat/conversa');

    }

    public function chatRequest(){
        $url = 'http://sobek.ufrgs.br/tools/chat-llm.php/chat';
        $data = [
                'modo' => 'pandasai', 
                'sessao' => 2,
                'source' => 'csv',
                #essa coleção deve ser variável de ambiente
                'colecao' => 'https://alojaif.canoas.ifrs.edu.br/public/CSV/6987if2131'
            ];
        $data['chat'] = $_POST['prompt'] ;

        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'content' => http_build_query($data)
            )
        );
        $context = stream_context_create($opts);
        $this->response(file_get_contents($url, false, $context));
    }

    private function localizaSituacao($situacao){
        $situacaoObj = Situacao::getOneByCondition(
            Prepared::condition(
                'situacao like ?',
                [$situacao]
            )
        );

        if(!$situacaoObj){
            return -1;
        }
        return $situacaoObj->getId();
    }

    private function localizarSala($bloco, $sala)
    {
        
        $salaObj = Sala::getOneByCondition(
            Prepared::condition(
                'sala = ? AND bloco= ?',
                [$sala, $bloco]
            )
        );

        if(!$salaObj){
            return -1;
        }
        return $salaObj->getId();
    }

    public function __destruct()
    {
        if(get_class($this->view) === 'VisualizadorGeral'){
            $this->view->addTemplate('rodape');
        }
        parent::__destruct();
    }

}
