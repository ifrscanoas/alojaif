<?php

class ControladorLogin extends AbstractController{

    public function __construct()
    {
        $this->view = new VisualizadorGeral();
    }
    
    public function validaLogin()
    {
        $_SESSION['principal'] = '#';
        if(isset($_POST["email"]) && isset($_POST["senha"])){
            $usuario = $_POST["email"];
            $senha = $_POST["senha"];

            $papel = Papel::getOneByCondition(Prepared::condition(
                'usuario = ? AND senha = ?',
                [$usuario, $senha]
            ));
            if($papel){
                $_SESSION['logado'] = $papel->getNomePapel();
                $_SESSION['principal'] = $papel->getRotaBase();
                $this->redirect($papel->getRotaBase());
                exit();
            }
        } 
        $this->redirect('/login?erro=true');   
    }

    public function logout()
    {
        unset($_SESSION['logado']);
        session_destroy();

        $this->redirect('/');
    }

    public function mantemVivo()
    {
        $this->view->renderAjax();
        if(isset($_SESSION['logado'])){
            echo "Tudo vai dar certo!";
        }else{
            echo 'redirect';
        }
    }

    public function index()
    {
        
       $this->view->setTitle('Abrigo IFRS');  
       $this->view->addCSS('login');
       $this->view->startForm( '/login/validaLogin');
       $this->view->addTemplate('login/login');
       $this->view->endForm(false);
    }
    
    public function paginaNaoEncontrada()
    {
        $this->view->setTitle('Pagina não encontrada'); 
    }

    
}