<?php

/**
 * Classe controladora referente ao objeto Situacao para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 12-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorSituacao extends ControladorAdmin
 {

    /**
     * @var SituacaoDAO
     */
    protected $model;

     /**
     * Construtor da classe Situacao e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new SituacaoDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Situação');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados('/admin//situacao/tabela');
        $tabela->setTitulo('Situação');
        $tabela->addAcaoAdicionar( 
        '/admin//situacao/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//situacao/editar');
        $tabela->addAcaoDeletar( 
        '/admin//situacao/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Situação', 'situacao');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Cor', 'cor');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Situação
     *
     * @param Situacao $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Situacao $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $situacao = $obj == null ? new Situacao() : $obj;

        $this->view->setTitle('Novo Situação');

        $this->view->attValue('situacao', $situacao);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//situacao/criarNovoFim' . $return);
        $this->view->addTemplate('forms/situacao');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Situacao $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Situacao $obj = null) 
    {
        if($obj == null){
            $situacao = $this->model->getById($id);
        }else{
            $situacao = $obj;
        }

        $this->view->setTitle('Editar Situação');

        $this->view->attValue('situacao', $situacao);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm('/admin//situacao/editarFim');
        $this->view->addTemplate('forms/situacao');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $situacao = new Situacao();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($situacao->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($situacao)){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Situacao: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($situacao);
    }

    /**
     * Controla a atualização dos objetos Situacao na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idSituacao', BASE_URL . '/admin//situacao/manter');
        $situacao = new Situacao();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $situacao->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($situacao->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($situacao)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Situacao: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $situacao);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $situacao = new Situacao();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $situacao->setId($id);
        try {
             if($this->model->delete($situacao) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Situacao: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }

    public function situacoes(){
        $this->view->renderAjax();
        echo JSON::encode(Situacao::getAll());
    }

    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }
    

}
