<?php

/**
 * Classe controladora referente ao objeto Sala para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 10-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorSala extends ControladorAdmin
 {

    /**
     * @var SalaDAO
     */
    protected $model;

     /**
     * Construtor da classe Sala e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new SalaDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Sala');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados('/admin//sala/tabela');
        $tabela->setTitulo('Sala');
        $tabela->addAcaoAdicionar( 
        '/admin//sala/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//sala/editar');
        $tabela->addAcaoDeletar( 
        '/admin//sala/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Bloco', 'bloco');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Sala', 'sala');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Capacidade', 'capacidade');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('double precision');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Sala
     *
     * @param Sala $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Sala $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $sala = $obj == null ? new Sala() : $obj;

        $this->view->setTitle('Novo Sala');

        $this->view->attValue('sala', $sala);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm('/admin//sala/criarNovoFim' . $return);
        $this->view->addTemplate('forms/sala');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Sala $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Sala $obj = null) 
    {
        if($obj == null){
            $sala = $this->model->getById($id);
        }else{
            $sala = $obj;
        }

        $this->view->setTitle('Editar Sala');

        $this->view->attValue('sala', $sala);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm('/admin//sala/editarFim');
        $this->view->addTemplate('forms/sala');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $sala = new Sala();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($sala->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($sala)){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Sala: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($sala);
    }

    /**
     * Controla a atualização dos objetos Sala na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idSala', BASE_URL . '/admin//sala/manter');
        $sala = new Sala();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $sala->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($sala->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($sala)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Sala: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $sala);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $sala = new Sala();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $sala->setId($id);
        try {
             if($this->model->delete($sala) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Sala: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }

    public function salas(){
        $this->view->renderAjax();
        echo JSON::encode(Sala::getAll());
    }

}
