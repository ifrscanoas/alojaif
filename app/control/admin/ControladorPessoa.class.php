<?php

/**
 * Classe controladora referente ao objeto Pessoa para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 23-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorPessoa extends ControladorAdmin
 {

    /**
     * @var PessoaDAO
     */
    protected $model;

     /**
     * Construtor da classe Pessoa e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new PessoaDAO();
    }

    public function getByCPF($cpf){
        $pessoa = Pessoa::getOneByCondition(
            Prepared::condition('cpf = ?',[$cpf]));
        if($pessoa){
            return $this->response($pessoa);
        }else{
            return $this->response(false, 500);
        }
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Pessoa');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//pessoa/tabela');
        $tabela->setTitulo('Pessoa');
        $tabela->addAcaoAdicionar( 
        '/admin//pessoa/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//pessoa/editar');
        $tabela->addAcaoDeletar( 
        '/admin//pessoa/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo pessoa id', 'tipo_pessoa_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Cpf', 'cpf');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Observação', 'observacao');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Sys', 'sys');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Pessoa
     *
     * @param Pessoa $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Pessoa $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $pessoa = $obj == null ? new Pessoa() : $obj;

        $this->view->setTitle('Novo Pessoa');

        $this->view->attValue('pessoa', $pessoa);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//pessoa/criarNovoFim' . $return);
        $this->view->addTemplate('forms/pessoa');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Pessoa $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Pessoa $obj = null) 
    {
        if($obj == null){
            $pessoa = $this->model->getById($id);
        }else{
            $pessoa = $obj;
        }

        $this->view->setTitle('Editar Pessoa');

        $this->view->attValue('pessoa', $pessoa);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//pessoa/editarFim');
        $this->view->addTemplate('forms/pessoa');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $pessoa = new Pessoa();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($pessoa->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($pessoa)){
                $this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Pessoa: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($pessoa);
    }

    /**
     * Controla a atualização dos objetos Pessoa na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idPessoa', BASE_URL . '/admin//pessoa/manter');
        $pessoa = new Pessoa();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $pessoa->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($pessoa->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($pessoa)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Pessoa: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $pessoa);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $pessoa = new Pessoa();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $pessoa->setId($id);
        try {
             if($this->model->delete($pessoa) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Pessoa: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('tipo_pessoa', 'tipo_pessoa_id, tipo_pessoa');
        $lista = $this->model->getMapaSimplesDados($consulta, 'tipo_pessoa_id', 'tipo_pessoa');
        $this->view->attValue('listaTipoPessoa', $lista);

    }

}
