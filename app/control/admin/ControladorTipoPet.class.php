<?php

/**
 * Classe controladora referente ao objeto Tipo_pet para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 17-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorTipoPet extends ControladorAdmin
 {

    /**
     * @var TipoPetDAO
     */
    protected $model;

     /**
     * Construtor da classe Tipo_pet e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new TipoPetDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Tipo pet');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//tipoPet/tabela');
        $tabela->setTitulo('Tipo pet');
        $tabela->addAcaoAdicionar( 
        '/admin//tipoPet/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//tipoPet/editar');
        $tabela->addAcaoDeletar( 
        '/admin//tipoPet/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo', 'tipo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Tipo pet
     *
     * @param TipoPet $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(TipoPet $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $tipoPet = $obj == null ? new TipoPet() : $obj;

        $this->view->setTitle('Novo Tipo pet');

        $this->view->attValue('tipoPet', $tipoPet);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//tipoPet/criarNovoFim' . $return);
        $this->view->addTemplate('forms/tipo_pet');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param TipoPet $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, TipoPet $obj = null) 
    {
        if($obj == null){
            $tipoPet = $this->model->getById($id);
        }else{
            $tipoPet = $obj;
        }

        $this->view->setTitle('Editar Tipo pet');

        $this->view->attValue('tipoPet', $tipoPet);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//tipoPet/editarFim');
        $this->view->addTemplate('forms/tipo_pet');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $tipoPet = new TipoPet();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($tipoPet->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($tipoPet)){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir TipoPet: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($tipoPet);
    }

    /**
     * Controla a atualização dos objetos TipoPet na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idTipoPet', BASE_URL . '/admin//tipoPet/manter');
        $tipoPet = new TipoPet();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $tipoPet->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($tipoPet->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($tipoPet)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar TipoPet: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $tipoPet);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $tipoPet = new TipoPet();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $tipoPet->setId($id);
        try {
             if($this->model->delete($tipoPet) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar TipoPet: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }

}
