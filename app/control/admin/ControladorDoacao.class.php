<?php

/**
 * Classe controladora referente ao objeto Doacao para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 23-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorDoacao extends ControladorAdmin
 {

    /**
     * @var DoacaoDAO
     */
    protected $model;

     /**
     * Construtor da classe Doacao e  inicializa o modelo de dados 
     *
     */
    public function __construct() 
    {
        parent::__construct();
        $this->model = new DoacaoDAO();
    }


    public function roupas()
    {
        $this->doacaoBase();
       
        $this->view->setTitle('Doação de roupas');
        
        $tiposRegistro = TipoRegistro::getSimpleMap('tipo', 'id >= 25 AND id<=41 ', 'id');
        $this->view->attValue('listaTipoRegistro', $tiposRegistro);

        $dao = new DoacaoDAO();
        $lista = $dao->getListaCompleta('tipo_registro_id >= 25 AND tipo_registro_id<=41 ');
        $this->view->attValue('doacoes', $lista);
    }

    public function historicoHigiene()
    {
        $this->doacaoBase();
        
        $this->view->setTitle('Doação de Higiene histórico');
        
        $tiposRegistro = TipoRegistro::getList('id = 6 OR id = 7 ', 'id', false, 'principal');
        $this->view->attValue('listaTipoRegistro', $tiposRegistro);

        $dao = new DoacaoDAO();
        $lista = $dao->getListaCompleta('tipo_registro_id = 6 OR tipo_registro_id = 7');
        $this->view->attValue('doacoes', $lista);
        $this->view->attValue('EXTRA', 'doacao/data.tpl');

    }

    public function higiene()
    {
        $this->doacaoBase();
        
        $this->view->setTitle('Doação de Higiene');
        
        $tiposRegistro = TipoRegistro::getList('id = 6 OR id = 7 ', 'id', false, 'principal');
        $this->view->attValue('listaTipoRegistro', $tiposRegistro);

        $dao = new DoacaoDAO();
        $lista = $dao->getListaCompleta('tipo_registro_id = 6 OR tipo_registro_id = 7');
        $this->view->attValue('doacoes', $lista);
    }

    public function adicionarAjax(){
        $dados = ValidatorUtil::sanitizeForm();
        $pessoa = $this->inserePessoa($dados);

        $doacao = new Doacao();
        $doacao->setArrayDados($dados);
        $doacao->setPessoaId($pessoa->getID());
        
        if($doacao->save()){
            return $this->response($doacao);
        }else{
            return $this->response('Problema ao inserir dados', 500);
        }

        
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Doação');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//doacao/tabela');
        $tabela->setTitulo('Doação');
        $tabela->addAcaoAdicionar( 
        '/admin//doacao/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//doacao/editar');
        $tabela->addAcaoDeletar( 
        '/admin//doacao/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Pessoa id', 'pessoa_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo registro id', 'tipo_registro_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Data registro', 'data_registro');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('timestamp without time zone');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Observação', 'observacao');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Quantidade', 'quantidade');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Doação
     *
     * @param Doacao $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Doacao $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $doacao = $obj == null ? new Doacao() : $obj;

        $this->view->setTitle('Novo Doação');

        $this->view->attValue('doacao', $doacao);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//doacao/criarNovoFim' . $return);
        $this->view->addTemplate('forms/doacao');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Doacao $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Doacao $obj = null) 
    {
        if($obj == null){
            $doacao = $this->model->getById($id);
        }else{
            $doacao = $obj;
        }

        $this->view->setTitle('Editar Doação');

        $this->view->attValue('doacao', $doacao);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//doacao/editarFim');
        $this->view->addTemplate('forms/doacao');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $doacao = new Doacao();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($doacao->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($doacao)){
                $this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Doacao: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($doacao);
    }

    /**
     * Controla a atualização dos objetos Doacao na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idDoacao', BASE_URL . '/admin//doacao/manter');
        $doacao = new Doacao();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $doacao->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($doacao->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($doacao)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Doacao: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $doacao);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $doacao = new Doacao();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $doacao->setId($id);
        try {
             if($this->model->delete($doacao) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Doacao: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('pessoa', 'pessoa_id, pessoa');
        $lista = $this->model->getMapaSimplesDados($consulta, 'pessoa_id', 'pessoa');
        $this->view->attValue('listaPessoa', $lista);

        $consulta = $this->model->queryTable('tipo_registro', 'tipo_registro_id, tipo_registro');
        $lista = $this->model->getMapaSimplesDados($consulta, 'tipo_registro_id', 'tipo_registro');
        $this->view->attValue('listaTipoRegistro', $lista);

    }

}
