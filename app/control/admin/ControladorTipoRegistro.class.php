<?php

/**
 * Classe controladora referente ao objeto Tipo_registro para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 15-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorTipoRegistro extends ControladorAdmin
 {

    /**
     * @var TipoRegistroDAO
     */
    protected $model;

     /**
     * Construtor da classe Tipo_registro e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new TipoRegistroDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Tipo registro');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//tipoRegistro/tabela');
        $tabela->setTitulo('Tipo registro');
        $tabela->addAcaoAdicionar( 
        '/admin//tipoRegistro/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//tipoRegistro/editar');
        $tabela->addAcaoDeletar( 
        '/admin//tipoRegistro/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo', 'tipo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Sys', 'sys');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('boolean');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Tipo registro
     *
     * @param TipoRegistro $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(TipoRegistro $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $tipoRegistro = $obj == null ? new TipoRegistro() : $obj;

        $this->view->setTitle('Novo Tipo registro');

        $this->view->attValue('tipoRegistro', $tipoRegistro);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//tipoRegistro/criarNovoFim' . $return);
        $this->view->addTemplate('forms/tipo_registro');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param TipoRegistro $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, TipoRegistro $obj = null) 
    {
        if($obj == null){
            $tipoRegistro = $this->model->getById($id);
        }else{
            $tipoRegistro = $obj;
        }

        $this->view->setTitle('Editar Tipo registro');

        $this->view->attValue('tipoRegistro', $tipoRegistro);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//tipoRegistro/editarFim');
        $this->view->addTemplate('forms/tipo_registro');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $tipoRegistro = new TipoRegistro();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($tipoRegistro->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($tipoRegistro)){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir TipoRegistro: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($tipoRegistro);
    }

    /**
     * Controla a atualização dos objetos TipoRegistro na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idTipoRegistro', BASE_URL . '/admin//tipoRegistro/manter');
        $tipoRegistro = new TipoRegistro();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $tipoRegistro->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($tipoRegistro->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($tipoRegistro)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar TipoRegistro: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $tipoRegistro);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $tipoRegistro = new TipoRegistro();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $tipoRegistro->setId($id);
        try {
             if($this->model->delete($tipoRegistro) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar TipoRegistro: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }

}
