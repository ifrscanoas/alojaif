<?php

/**
 * Classe controladora referente ao objeto Pet para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 17-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorPet extends ControladorAdmin
 {

    /**
     * @var PetDAO
     */
    protected $model;

     /**
     * Construtor da classe Pet e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new PetDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

    public function todos(){
        $this->view->setTitle('Pet');
        $this->view->addTemplate('pet/todos_pet');

        $dao = new PetDAO();
        $this->view->attValue('pets', $dao->getAllByAbrigados());

        $this->view->attValue('tipos', TipoPet::getMap());

        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaPets');
        $dataTable->addConf("bPaginate", false);
        $this->view->addComponente($dataTable);
        
    }


     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Pet');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//pet/tabela');
        $tabela->setTitulo('Pet');
        $tabela->addAcaoAdicionar( 
        '/admin//pet/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//pet/editar');
        $tabela->addAcaoDeletar( 
        '/admin//pet/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Abrigado id', 'abrigado_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo', 'tipo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Nome', 'nome');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Raca', 'raca');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Descrição', 'descricao');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Foto', 'foto');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Perdido', 'perdido');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('boolean');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Resgatado', 'resgatado');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('boolean');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Sexo', 'sexo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Vacinas', 'vacinas');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('boolean');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo pet id', 'tipo_pet_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }

    public function  criarNovoPorAbrigado(int $id)
    {
        $this->criarNovo();
        $this->view->attValueJS('tutorID', $id);
        
    }

    /**
     * Controla a inserção de um novo registro em Pet
     *
     * @param Pet $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Pet $obj = null)
     {
        $pet = $obj == null ? new Pet() : $obj;

        $this->view->setTitle('Novo Pet');

        $this->view->attValue('pet', $pet);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//pet/criarNovoFim' );
        $this->view->addTemplate('forms/pet');
        $this->view->endForm();
        
        $this->view->attValueJS('tutorID', -1);
        
        $this->view->addJS('pets');
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Pet $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Pet $obj = null) 
    {
        if($obj == null){
            $pet = $this->model->getById($id);
        }else{
            $pet = $obj;
        }

        $this->view->setTitle('Editar Pet');

        $this->view->attValue('pet', $pet);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin/pet/editarFim');
        $this->view->addTemplate('forms/pet');
        $this->view->endForm();

        $this->view->addJS('pets');
        $this->view->attValueJS('tutorID', $pet->getAbrigadoId());
        
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $pet = new Pet();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if(isset($_FILES["foto"]) && $_FILES["foto"]["tmp_name"]){

                $caminho_temporario = $_FILES["foto"]["tmp_name"];
                $dados["foto"] = $this->uploadFoto($caminho_temporario);;
            }

            if($pet->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($pet)){
                //$this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Pet: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($pet);
    }

    /**
     * Controla a atualização dos objetos Pet na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idPet', BASE_URL . '/admin//pet/manter');
        $pet = new Pet();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $pet->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if(isset($_FILES["foto"]) && $_FILES["foto"]["tmp_name"]){

                $caminho_temporario = $_FILES["foto"]["tmp_name"];
                $dados["foto"] = $this->uploadFoto($caminho_temporario);;
            }

            if ($pet->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($pet)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Pet: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $pet);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $pet = new Pet();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $pet->setId($id);
        try {
             if($this->model->delete($pet) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Pet: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }

    
    public function getFoto($id){
        $pet = Pet::getOne($id);
        $path = $pet->getFoto();

        header ('Content-type: image/png');
        readfile($path);
        exit();
    }

    public function getMiniatura($id){
        $pet = Pet::getOne($id);
        $path = $pet->getFoto();

        $this->geraMiniatura($path);
    }

    public function nucleoFamiliarPDF(){
        //Ver da imagem em base64 https://stackoverflow.com/questions/50988908/php-imageflip-usage-with-mpdf
        $this->view->resetTemplates();
        $this->nucleoFamiliar();
        $result = $this->view->resultAsString();

        $mpdf = new PDF();

        $mpdf->WriteHTML($result);
        $mpdf->Output();
    }

    public function nucleoFamiliar(){
        $this->view->setTitle('Núcleo Familiar');
        $this->view->addTemplate('abrigados/familia');
        $this->view->attValue('EXTRA', 'pet/familia.tpl');

        $this->view->addCSS('print', 'print');

        $this->view->attValue('salas', Sala::getMap());

        $dao =  new AbrigadoDAO();

        $this->view->attValue('familias', $dao->getFamilias());
        $this->view->attValue('semFamilia', $dao->getAbrigadosSemFamilia());

        $dao = new PetDAO();
        $this->view->attValue('pets', $dao->getMapByAbrigados());

        $this->view->attValue('tipos', TipoPet::getMap());
    }

    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('abrigado', 'id, nome_completo');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'nome_completo');
        $this->view->attValue('listaAbrigado', $lista);

        $consulta = $this->model->queryTable('tipo_pet', 'id, tipo_pet');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'tipo_pet');
        $this->view->attValue('listaTipoPet', $lista);

    }

}
