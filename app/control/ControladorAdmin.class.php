<?php
class ControladorAdmin extends ControladorGeral{

    protected function doacaoBase()
    {
        $this->view->addJS('doacoes');
        $this->view->addTemplate('doacao/tela');
        
        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaDoacoes');
        $dataTable->addConf("bPaginate", false);
        $this->view->addComponente($dataTable);
        
        $this->view->attValue('listaTipoPessoa', TipoPessoa::getMap());

        $this->view->attValue('ACAO_FORM', '/admin/doacao/adicionarAjax');
        
    }

    /**
     * Undocumented function
     *
     * @param array $dados
     * @return Pessoa
     */
    protected function inserePessoa($dados){
        $pessoa = new Pessoa();
        if($dados['pessoaId'] != -1){
            $pessoa->setId($dados['pessoaId']);
        }else{
            $pessoa->setArrayDados($dados);
            $pessoa->save(true);
        }
        //ds($pessoa);

        return $pessoa;
    }

  
    public function uploadFoto($caminho_temporario){
        $hash_md5 = md5_file($caminho_temporario);
        $diretorio_destino = CACHE . "uploads/";
        $caminho_destino = $diretorio_destino . $hash_md5 . ".jpg";
        ds($caminho_destino);
        move_uploaded_file($caminho_temporario, $caminho_destino);
        return $caminho_destino;
    }

    public function geraMiniatura($path){
        $miniPath = str_replace('.jpg', '_mini.jpg', $path);
        
        if(!file_exists($miniPath)){
            new Redimensionador($path, $miniPath, 100, 100);
            list($largura, $altura, $tipo) = getimagesize($miniPath);

            if($largura > $altura){
                // Load
                $source = imagecreatefromjpeg($miniPath);
        
                // Rotate
                $rotate = imagerotate($source, -90, 0);
        
                // Output
                imagejpeg($rotate, $miniPath);
        
                // Free the memory
                imagedestroy($source);
                imagedestroy($rotate);
            }
        
        }
        
        header ('Content-type: image/jpg');
        readfile($miniPath);
     
        exit();
    }
}