INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '2', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '3', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '4', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '5', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '6', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '7', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '8', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '9', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '10', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '11', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '101', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'F', '102', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'Caminhão', '', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'B', '1', 20 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'C', '1', 50 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'D', '1', 15 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'D', '2', 15 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'D', '3', 15 );
INSERT INTO sala ( bloco, sala, capacidade) VALUES ( 'D', '4', 15 );

INSERT INTO sala (id, bloco, sala, capacidade) VALUES ( -1, 'NÃO INFORMADO', '', 0 );



INSERT INTO situacao (id, situacao, cor) VALUES ( 1, 'SAÍDA TEMPORÁRIA', 'saida');
INSERT INTO situacao (id, situacao, cor) VALUES ( 2, 'ABRIGADO',  'abrigado');
INSERT INTO situacao (id, situacao, cor) VALUES ( 3, 'DESOCUPOU', 'desocupou' );
INSERT INTO situacao (id, situacao, cor) VALUES ( 4, 'TRABALHO', 'trabalho');
INSERT INTO situacao (id, situacao, cor) VALUES ( 5, 'AMBULATÓRIO',  'ambulatorio');
INSERT INTO situacao (id, situacao, cor) VALUES ( 6, 'HOSPITAL',  'hospital');
INSERT INTO situacao (id, situacao, cor) VALUES ( 7, 'ÓBITO',  'obito');
INSERT INTO situacao (id, situacao, cor) VALUES ( 8, 'MOVIDO',  'movido');
INSERT INTO situacao (id, situacao, cor) VALUES ( 9, 'VERIFICAR', 'verificar' );
INSERT INTO situacao (id, situacao, cor) VALUES ( 12, 'BANHO',  'banho');
INSERT INTO situacao (id, situacao, cor) VALUES ( 13, 'TESTE', 'teste');
SELECT setval('situacao_id_seq', 11);

INSERT INTO tipo_registro (id, tipo, sys) VALUES (1, 'SAÍDA TEMPORÁRIA', true);
INSERT INTO tipo_registro (id, tipo, sys) VALUES (3, 'ALMOÇO', true);
INSERT INTO tipo_registro (id, tipo, sys) VALUES (4, 'JANTA', true);
INSERT INTO tipo_registro (id, tipo, sys) VALUES (11, 'SAÍDA TRABALHO', true);
INSERT INTO tipo_registro (id, tipo, sys) VALUES (12, 'BANHO', true);
INSERT INTO tipo_registro (id, tipo) VALUES (5, 'RETIRADA DE ALIMENTOS');
INSERT INTO tipo_registro (id, tipo) VALUES (6, 'RETIRADA DE PRODUTOS LIMPEZA');
INSERT INTO tipo_registro (id, tipo) VALUES (7, 'RETIRADA DE PRODUTOS HIGIENE');
INSERT INTO tipo_registro (id, tipo) VALUES (8, 'RETIRADA DE ROUPAS');
INSERT INTO tipo_registro (id, tipo) VALUES (9, 'NECESSITA DOCUMENTOS');
INSERT INTO tipo_registro (id, tipo) VALUES (10, 'OCORRÊNCIAS POLICIAS');
--Módulo UPA =]
INSERT INTO public.tipo_registro(id, tipo, sys)	VALUES (16, 'MEDICAMENTOS (MUC)', false);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (17, 'ENFERMARIA', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (18, 'COMORBIDADES', false);
INSERT INTO public.tipo_registro(id, tipo, sys)	VALUES (19, 'ALERGIAS', false);
INSERT INTO public.tipo_registro(id, tipo, sys)	VALUES (20, 'REGISTRO MÉDICO', true); -- Apenas médico enxerga  e relatório final
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (21, 'ENTREGA MEDICAMENTO', TRUE); --???
--Módulo portaria
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (22, 'SAIU PORTARIA', TRUE); 
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (23, 'ENTROU PORTARIA', TRUE); 
--módulo roupas
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (25, 'CASACO/BLUSÃO', TRUE); 
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (26, 'CALÇA', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (27, 'CALÇADOS', TRUE); 
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (28, 'CALCINHA/CUECA', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (29, 'SUTIÃ', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (30, 'MEIA', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (31, 'CAMISA CURTA', TRUE);  
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (32, 'CAMISA LONGA', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (33, 'SAIA/VESTIDO', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (34, 'SHORT/BERMUDA', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (35, 'COBERTOR', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (36, 'PIJAMA', TRUE);   
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (37, 'TOALHA', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (38, 'LENÇOL', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (39, 'FRONHA', TRUE);   
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (40, 'TRAVESSEIRO', TRUE);
INSERT INTO public.tipo_registro(id, tipo, sys) VALUES (41, 'ACESSÓRIOS', TRUE);

SELECT setval('tipo_registro_id_seq', 50);

--View para facilitar buscas
DROP VIEW IF EXISTS registro_completo;
CREATE VIEW registro_completo AS SELECT tp.tipo, tp.private, tp.sys, r.id as principal, r.* FROM registro r 
        INNER JOIN tipo_registro tp ON tp.id = r.tipo_registro_id; 

--Tipos voluntarios
INSERT INTO tipo_voluntario(id, tipo) VALUES (1, 'Servidor');
INSERT INTO tipo_voluntario(id, tipo) VALUES (2, 'Voluntário externo/comunidade');
INSERT INTO tipo_voluntario(id, tipo) VALUES (3, 'Voluntário aluno');
INSERT INTO tipo_voluntario(id, tipo) VALUES (4, 'Voluntário ex-aluno');
INSERT INTO tipo_voluntario(id, tipo) VALUES (5, 'Voluntário terceirizado');
INSERT INTO tipo_voluntario(id, tipo) VALUES (6, 'Voluntário abrigado - interno');
INSERT INTO tipo_voluntario(id, tipo) VALUES (7, 'Voluntário abrigado - externo');
SELECT setval('tipo_voluntario_id_seq', 11);

SELECT setval('voluntario_id_seq', 10000); --Setando para deixar os primeiros registros para importação


INSERT INTO tipo_pet(id, tipo) VALUES (1, 'CACHORRO');
INSERT INTO tipo_pet(id, tipo) VALUES (2, 'GATO');
INSERT INTO tipo_pet(id, tipo) VALUES (3, 'COELHO');
INSERT INTO tipo_pet(id, tipo) VALUES (4, 'PORQUINHO DA INDIA');
INSERT INTO tipo_pet(id, tipo) VALUES (5, 'CALOPSITA');
INSERT INTO tipo_pet(id, tipo) VALUES (6, 'CAVALO');

--Modulo papeis v1
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (1, 'admin', 'sandrinho', 'admin', '/admin/abrigado/tudo/0' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (2, 'gestao', 'admin', 'admin', '/admin/abrigado/estatisticas' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (3, 'recepcao', 'r2d2', 'voluntario', '/admin/abrigado/telaGeral/0' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (4, 'enfermagem', 'r2d2', 'roupas', '/admin/abrigado/enfermaria' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (5, 'medicamentos', 'r2d2', 'roupas', '/admin/abrigado/medicos' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (6, 'portaria', 'r2d2', 'portaria', '/portaria' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (7, 'roupas', 'r2d2', 'roupas', '/admin/abrigado/roupas' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (8, 'higiene', 'r2d2', 'higiene', '/admin/abrigado/higiene' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (9, 'banho', 'r2d2', 'banho', '/admin/abrigado/banho' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (10, 'voluntario', 'r2d2', 'voluntario', '/admin/abrigado/telaGeral/0' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (11, 'alimentacao', 'r2d2', 'alimentacao', '/admin/abrigado/refeicao' );
INSERT INTO papel (id, usuario, senha, nome_papel, rota_base) VALUES (12, 'veterinaria', 'r2d2', 'veterinaria', '/admin/pet/todos' );
SELECT setval('papel_id_seq', 15);

-- Modulo doacoes
INSERT INTO tipo_pessoa (id, tipo_pessoa, cor) VALUES ( 1, 'Aluno', 'blue');
INSERT INTO tipo_pessoa (id, tipo_pessoa, cor) VALUES ( 2, 'Ex-aluno', 'blue');
INSERT INTO tipo_pessoa (id, tipo_pessoa, cor) VALUES ( 3, 'Servidor', 'blue');
INSERT INTO tipo_pessoa (id, tipo_pessoa, cor) VALUES ( 4, 'Terceirizado', 'blue');
INSERT INTO tipo_pessoa (id, tipo_pessoa, cor) VALUES ( 5, 'Prefeitura', 'blue');
INSERT INTO tipo_pessoa (id, tipo_pessoa, cor) VALUES ( 6, 'Voluntário', 'blue');
INSERT INTO tipo_pessoa (id, tipo_pessoa, cor) VALUES ( 7, 'Comunidade externa', 'blue');
INSERT INTO tipo_pessoa (id, tipo_pessoa, cor) VALUES ( 10, 'Empresa', 'blue');


SELECT setval('tipo_pessoa_id_seq', 11);


DROP VIEW IF EXISTS doacao_completa;
CREATE VIEW doacao_completa AS 
SELECT tp.tipo, tp.private, tp.sys, p.nome_completo, p.cpf,
         d.* FROM doacao d 
        INNER JOIN tipo_registro tp ON tp.id = d.tipo_registro_id 
        INNER JOIN pessoa p ON p.id = d.pessoa_id; 

SELECT setval('pessoa_id_seq', 10000); --Setando para deixar os primeiros registros para importação
SELECT setval('doacao_id_seq', 10000); --Setando para deixar os primeiros registros para importação

INSERT INTO tipo_doacao (id, tipo_doacao, cor) VALUES ( 1,    'PIX',    'blue'  );

SELECT setval('tipo_doacao_id_seq', 10000); --Setando para deixar os primeiros registros para importação

DROP VIEW IF EXISTS doacao_recebimento_completa;
CREATE VIEW doacao_recebimento_completa AS 
SELECT tp.tipo_doacao as tipo, p.nome_completo, p.cpf,
         d.* FROM doacao_recebimento d 
        INNER JOIN tipo_doacao tp ON tp.id = d.tipo_doacao_id 
        INNER JOIN pessoa p ON p.id = d.pessoa_id; 


--Abrigados padrões e de teste
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (2840, 'ANAIR SILVA WAGNER', NULL, NULL, '3049558831', NULL, '/app/z_data/uploads/3ad71865a07d14c88d3322bb33b4b41c.jpg', '1946-03-19 00:00:00', 78, NULL, 'MATHIAS VELHO', NULL, NULL, NULL, 2, 15, NULL);
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (2530, 'ADRIANA SANTOS DE MORAES DIAS', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 3, 14, NULL);
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (2540, 'ADRIANO PINTO DE VARGAS', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 3, 11, NULL);
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (2560, 'AGATHA SOARES OLIVER', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 3, 14, NULL);
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (3200, 'CELI SOUZA QUEEN ', NULL, NULL, '9080463591', NULL, '/app/z_data/uploads/18799f43e65de5a9c87847c694f8f3f2.jpg', '1986-09-17 00:00:00', 38, NULL, 'MATHIAS VELHO', NULL, NULL, NULL, 2, 15, NULL);
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (2590, 'ALDREI CLARK', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 3, 6, NULL);
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (2600, 'ALESSANDRA DE OLIVEIRA SKYWALKER', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'MATHIAS VELHO', NULL, NULL, '2024-05-09 00:00:00', 3, 10, NULL);
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (2620, 'PEDRO GRIGOLLO', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 3, 14, NULL);
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (2630, 'PAULO HANKS FREIRE', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 3, 10, NULL);
INSERT INTO public.abrigado (id, nome_completo, cpf, observacao, documento, cartao_sus, foto, data_nascimento, idade_old, endereco, bairro, telefone, saida_temporaria, data_saida, situacao_id, sala_id, nome_pai) VALUES (2640, 'ALEXIA MULLEN DOS SANTOS', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 3, 7, NULL);

SELECT setval('abrigado_id_seq', 10000); --Setando para deixar os primeiros registros para importação

