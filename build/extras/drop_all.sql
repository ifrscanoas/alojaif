

--Apagando as views
DO $$ DECLARE
    r RECORD;
BEGIN
    FOR r IN (SELECT schemaname FROM pg_views WHERE schemaname = current_schema()) LOOP
        EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.schemaname) || ' CASCADE';
    END LOOP;
END $$;

--Apagando as tabelas
DO $$ DECLARE
    r RECORD;
BEGIN
    FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
        EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
    END LOOP;
END $$;

--Apagando as procedures e functions
DO $$ DECLARE
    r RECORD;
BEGIN
    FOR r IN (SELECT p.proname as name, p.proargtypes AS args FROM  pg_catalog.pg_namespace n JOIN pg_catalog.pg_proc p ON  p.pronamespace = n.oid  AND n.nspname =  current_schema()) LOOP
        EXECUTE 'DROP FUNCTION IF EXISTS ' || quote_ident(r.name) || '(' || oidvectortypes(r.args) ||')';
    END LOOP;
END $$;

