
CREATE TABLE abrigado
(
  id               bigserial NOT NULL,
  nome_completo    varchar   NOT NULL,
  cpf              varchar  ,
  observacao       varchar  ,
  documento        varchar  ,
  cartao_sus       varchar  ,
  foto             varchar  ,
  data_nascimento  varchar  ,
  nome_pai         varchar  ,
  nome_mae         varchar  ,
  idade_old        int      ,
  endereco         varchar  ,
  bairro           varchar  ,
  telefone         varchar  ,
  saida_temporaria varchar  ,
  data_saida       TIMESTAMP,
  situacao_id      int       NOT NULL,
  sala_id          int       NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE compra
(
  id                       bigserial         NOT NULL,
  nota_fiscal              character varying,
  data_compra              timestamp         NOT NULL DEFAULT now(),
  valor                    double precision  NOT NULL,
  quantidade               int              ,
  item                     character varying,
  observacao               character varying,
  razao_social_beneficiado character varying,
  nome_fantasia            character varying,
  PRIMARY KEY (id)
);

CREATE TABLE doacao
(
  id               bigserial NOT NULL,
  pessoa_id        bigint    NOT NULL,
  tipo_registro_id int       NOT NULL,
  data_registro    timestamp DEFAULT now(),
  observacao       varchar  ,
  quantidade       varchar   DEFAULT 1,
  PRIMARY KEY (id)
);

CREATE TABLE doacao_recebimento
(
  id               bigserial        NOT NULL,
  pessoa_id        bigint           NOT NULL,
  tipo_doacao_id   bigint           NOT NULL,
  data_recebimento timestamp        NOT NULL DEFAULT now(),
  valor            double precision NOT NULL DEFAULT 0,
  quantidade       int              NOT NULL DEFAULT 1,
  obs              varchar         ,
  PRIMARY KEY (id)
);

CREATE TABLE escala
(
  id             bigserial NOT NULL,
  voluntario_id  bigint    NOT NULL,
  turno_id       int       NOT NULL,
  dia_referencia date     ,
  PRIMARY KEY (id)
);

CREATE TABLE funcao
(
  id     serial  NOT NULL,
  funcao varchar,
  PRIMARY KEY (id)
);

CREATE TABLE papel
(
  id         serial  NOT NULL,
  usuario    varchar,
  senha      varchar,
  nome_papel varchar,
  rota_base  varchar,
  PRIMARY KEY (id)
);

CREATE TABLE papel_rota
(
  rota_id  serial NOT NULL,
  papel_id serial NOT NULL,
  PRIMARY KEY (rota_id, papel_id)
);

CREATE TABLE pessoa
(
  id             bigserial NOT NULL,
  tipo_pessoa_id bigint    NOT NULL,
  nome_completo  varchar  ,
  cpf            varchar  ,
  observacao     varchar  ,
  PRIMARY KEY (id)
);

CREATE TABLE pet
(
  id          bigserial NOT NULL,
  abrigado_id BIGINT    NOT NULL,
  tipo_pet_id int       NOT NULL,
  tipo        varchar  ,
  cor         varchar  ,
  nome        varchar  ,
  raca        varchar  ,
  sexo        varchar  ,
  vacinas     boolean  ,
  descricao   varchar  ,
  foto        varchar  ,
  perdido     boolean  ,
  resgatado   boolean  ,
  PRIMARY KEY (id)
);

CREATE TABLE presenca
(
  id            bigserial NOT NULL,
  voluntario_id bigint    NOT NULL,
  funcao_id     bigint    NOT NULL,
  data_registro timestamp,
  entrada       timestamp,
  saida         timestamp,
  almoco        boolean  ,
  janta         boolean  ,
  PRIMARY KEY (id)
);

CREATE TABLE registro
(
  id               bigserial NOT NULL,
  abrigado_id      bigint    NOT NULL,
  tipo_registro_id int       NOT NULL,
  observacao       varchar  ,
  tamanho          varchar  ,
  quantidade       int      ,
  data_registro    timestamp DEFAULT now(),
  valido           boolean  ,
  PRIMARY KEY (id)
);

CREATE TABLE responsavel
(
  id                  bigserial NOT NULL,
  abrigado_id         bigint    NOT NULL,
  responsavel_id      bigint    NOT NULL,
  tipo_responsavel_id bigint    NOT NULL,
  observacao          varchar  ,
  PRIMARY KEY (id)
);

CREATE TABLE rota
(
  id          serial  NOT NULL,
  rota        varchar,
  icone       varchar,
  label_menut varchar,
  PRIMARY KEY (id)
);

CREATE TABLE sala
(
  id         serial           NOT NULL,
  bloco      varchar         ,
  sala       varchar         ,
  capacidade double precision,
  PRIMARY KEY (id)
);

CREATE TABLE situacao
(
  id       serial  NOT NULL,
  situacao varchar,
  cor      varchar,
  PRIMARY KEY (id)
);

CREATE TABLE tipo_doacao
(
  id          bigserial         NOT NULL,
  tipo_doacao character varying,
  cor         character varying,
  PRIMARY KEY (id)
);

CREATE TABLE tipo_pessoa
(
  id          bigserial NOT NULL,
  tipo_pessoa varchar  ,
  cor         varchar  ,
  PRIMARY KEY (id)
);

CREATE TABLE tipo_pet
(
  id   serial  NOT NULL,
  tipo varchar,
  PRIMARY KEY (id)
);

CREATE TABLE tipo_registro
(
  id        serial  NOT NULL,
  tipo      varchar,
  sys       boolean NOT NULL DEFAULT false,
  private   boolean NOT NULL DEFAULT false,
  descricao varchar,
  cor       varchar,
  PRIMARY KEY (id)
);

COMMENT ON COLUMN tipo_registro.sys IS 'tipos de registros que servem apenas para auxilio e registro de estatus';

CREATE TABLE tipo_responsavel
(
  id   bigserial   NOT NULL,
  tipo responsavel,
  PRIMARY KEY (id)
);

CREATE TABLE tipo_voluntario
(
  id   bigserial NOT NULL,
  tipo varchar   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE turno
(
  id           serial  NOT NULL,
  turno        varchar,
  hora_inicial time   ,
  hora_final   time   ,
  PRIMARY KEY (id)
);

CREATE TABLE voluntario
(
  id                 bigserial NOT NULL,
  tipo_voluntario_id bigint    NOT NULL,
  nome_completo      varchar  ,
  cpf                varchar  ,
  telefone           varchar  ,
  foto               varchar  ,
  obs                varchar  ,
  email              varchar  ,
  PRIMARY KEY (id)
);

ALTER TABLE abrigado
  ADD CONSTRAINT FK_situacao_TO_abrigado
    FOREIGN KEY (situacao_id)
    REFERENCES situacao (id);

ALTER TABLE abrigado
  ADD CONSTRAINT FK_sala_TO_abrigado
    FOREIGN KEY (sala_id)
    REFERENCES sala (id);

ALTER TABLE pet
  ADD CONSTRAINT FK_abrigado_TO_pet
    FOREIGN KEY (abrigado_id)
    REFERENCES abrigado (id);

ALTER TABLE registro
  ADD CONSTRAINT FK_abrigado_TO_registro
    FOREIGN KEY (abrigado_id)
    REFERENCES abrigado (id);

ALTER TABLE registro
  ADD CONSTRAINT FK_tipo_registro_TO_registro
    FOREIGN KEY (tipo_registro_id)
    REFERENCES tipo_registro (id);

ALTER TABLE presenca
  ADD CONSTRAINT FK_voluntario_TO_presenca
    FOREIGN KEY (voluntario_id)
    REFERENCES voluntario (id);

ALTER TABLE presenca
  ADD CONSTRAINT FK_funcao_TO_presenca
    FOREIGN KEY (funcao_id)
    REFERENCES funcao (id);

ALTER TABLE responsavel
  ADD CONSTRAINT FK_abrigado_TO_responsavel
    FOREIGN KEY (abrigado_id)
    REFERENCES abrigado (id);

ALTER TABLE responsavel
  ADD CONSTRAINT FK_abrigado_TO_responsavel1
    FOREIGN KEY (responsavel_id)
    REFERENCES abrigado (id);

ALTER TABLE voluntario
  ADD CONSTRAINT FK_tipo_voluntario_TO_voluntario
    FOREIGN KEY (tipo_voluntario_id)
    REFERENCES tipo_voluntario (id);

ALTER TABLE responsavel
  ADD CONSTRAINT FK_tipo_responsavel_TO_responsavel
    FOREIGN KEY (tipo_responsavel_id)
    REFERENCES tipo_responsavel (id);

ALTER TABLE pet
  ADD CONSTRAINT FK_tipo_pet_TO_pet
    FOREIGN KEY (tipo_pet_id)
    REFERENCES tipo_pet (id);

ALTER TABLE doacao
  ADD CONSTRAINT FK_pessoa_TO_doacao
    FOREIGN KEY (pessoa_id)
    REFERENCES pessoa (id);

ALTER TABLE pessoa
  ADD CONSTRAINT FK_tipo_pessoa_TO_pessoa
    FOREIGN KEY (tipo_pessoa_id)
    REFERENCES tipo_pessoa (id);

ALTER TABLE escala
  ADD CONSTRAINT FK_voluntario_TO_escala
    FOREIGN KEY (voluntario_id)
    REFERENCES voluntario (id);

ALTER TABLE escala
  ADD CONSTRAINT FK_turno_TO_escala
    FOREIGN KEY (turno_id)
    REFERENCES turno (id);

ALTER TABLE papel_rota
  ADD CONSTRAINT FK_rota_TO_papel_rota
    FOREIGN KEY (rota_id)
    REFERENCES rota (id);

ALTER TABLE papel_rota
  ADD CONSTRAINT FK_papel_TO_papel_rota
    FOREIGN KEY (papel_id)
    REFERENCES papel (id);

ALTER TABLE doacao_recebimento
  ADD CONSTRAINT FK_pessoa_TO_doacao_recebimento
    FOREIGN KEY (pessoa_id)
    REFERENCES pessoa (id);

ALTER TABLE doacao
  ADD CONSTRAINT FK_tipo_registro_TO_doacao
    FOREIGN KEY (tipo_registro_id)
    REFERENCES tipo_registro (id);

ALTER TABLE doacao_recebimento
  ADD CONSTRAINT FK_tipo_doacao_TO_doacao_recebimento
    FOREIGN KEY (tipo_doacao_id)
    REFERENCES tipo_doacao (id);
